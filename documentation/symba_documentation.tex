\documentclass[a4paper,12pt]{article}

%nicer layout
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,spacing=true,factor=1100,stretch=10,shrink=10]{microtype}

\newcommand{\pipe}{\href{https://bitbucket.org/M_Janssen/symba}{\mbox{\textsc{symba}}}}

\usepackage{listings}

\usepackage{setspace}
\onehalfspacing

\usepackage{amsmath}

\usepackage{longtable}

%better margins
\usepackage{microtype}

%for \uline - underline with line breaks
\usepackage{ulem}

%outer enumeration can start with letters
\usepackage{enumitem}

\usepackage{listings}
\lstset{breaklines=true, frame=single, basicstyle=\scriptsize}

\usepackage[top=55pt,bottom=55pt,left=64pt,right=62pt]{geometry}
\emergencystretch 6em%

\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={blue!90!black},
    urlcolor={green!60!black}
}

\usepackage{rotating} %for vertical tables
\usepackage{caption,tabularx,booktabs}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

%skip items in enumerations
\makeatletter
\newcommand{\skipitems}[1]{%
  \addtocounter{\@enumctr}{#1}%
}
\makeatother

\title{SYnthetic Measurement creator for long Baseline Arrays
\href{https://bitbucket.org/M_Janssen/symba}{(\textsc{\pipe{}})}
documentation}
\author{Michael Janssen \& Freek Roelofs}
\date{\today}

\begin{document}

\maketitle

\tableofcontents

\newpage
\section{Preface}
\label{sec:intro}

The purpose of the end-to-end \pipe{} pipeline is to generate realistic synthetic very long baseline interferometry (VLBI) data. \pipe{} simulates the full data path -- starting with a source model, propagation effects through the atmosphere and the interferometry process with added quantization noise are simulated. The resultant `corrupted' data is then passed through the \href{https://bitbucket.org/M_Janssen/picard}{\mbox{\textsc{Rpicard}}} calibration pipeline and \href{https://github.com/achael/eht-imaging}{\mbox{\textsc{eht-imaging}}} is used to reconstruct the source model from the calibrated data.
There is a wide range of applications for these type of synthetic observations. To name a few examples:
\begin{itemize}
\item Direct model-data comparisons.
\item Studying how well specific features can be extracted from observational data.
\item Imaging and feature extraction parameter surveys through training data sets.
\item Understanding instruments, data properties, and calibration processes.
\item Optimizing calibration strategies.
\item Surveying the effect of weather conditions on the data (for go/no-go observing decisions for example).
\item Determining optimal locations for future stations that could be added to a VLBI array.
\item Investigating required array sensitivity upgrades to reach specific science goals.
\item Didactic purposes; i.e. teaching students about the complete VLBI data acquisition process.
\end{itemize}
More details about what \pipe{} can do are given in \href{https://doi.org/10.1051/0004-6361/201936622}{Roelofs \& Janssen et al., 2019, A\&A, 636, A5}. The document at hand describes how to use the \pipe{} pipeline.

\newpage
\section{Input}
\label{sec:input}
A single configuration file is used for every run of the pipeline. An example is given by \href{https://bitbucket.org/M_Janssen/symba/src/master/master_input.txt}{symba/master\_input.txt} in the repository.
The most important \textit{configuration options} are
\begin{itemize}
\item \textit{ms\_antenna\_table} -- A table containing all instrumental and environmental information for each antenna in the VLBI array. The stations given in this file do not have to exist in the real world, i.e., planned future stations can be added here. The following parameters can be set in the separate columns of the txt file:
\begin{enumerate}
\item \texttt{station} -- The name of the observatory.
\item \texttt{s\_rx} -- The noise from the receiver in Jansky. This value is given by the receiver temperature divided by the station's DPFU (`degrees-per-flux-unit' gain).
\item \texttt{pwv} -- The amount of precipitable water at zenith above the station in millimetres. This sets the amount of opacity-induced signal attenuation.
\item \texttt{gpress} -- The ground pressure in millibar.
\item \texttt{gtemp} -- The ground temperature in Kelvin.
\item \texttt{c\_time} -- The coherence time for atmospheric phase fluctuations in seconds.
\item \texttt{ptg\_rms} -- Root mean square pointing offsets in arcseconds. Used when \textit{pointing\_enabled = True}, see \texttt{PB\_FWHM230} below.
\item \texttt{PB\_FWHM230} -- The size of the telescope's primary beam in arcseconds at a frequency of 230\,GHz. For an observing frequency $\nu_\mathrm{obs}$, the code will scale the primary beam sizes by a factor of $\nu_\mathrm{obs}/(230\,\mathrm{GHz})$. If, \textit{pointing\_enabled = True}, the beam sizes are used for the simulated pointing model, which is controlled by the \mbox{\textit{pointing\_rms2offset}}, \textit{Nscan\_repoint}, and \textit{Nscan\_pointing\_grow} parameters. This pointing model simulates signal losses both on short (set by \texttt{c\_time}) and long (set by re-pointing every \textit{Nscan\_repoint} scans) timescales.
\item \texttt{PB\_model} -- The model for the primary beam profile. The default is Gaussian.
\item \texttt{ap\_eff} -- The aperture efficiency. Together with the \texttt{dish\_diameter} (see below), this sets the DPFU.
\item \texttt{gainR\_real} -- Real part of gain errors of the RCP receiver. Applied if \mbox{\textit{uvjones\_g\_on = True}}.
\item \texttt{gainR\_imag} -- Imaginary part of gain errors of the RCP receiver. Applied if \mbox{\textit{uvjones\_g\_on = True}}.
\item \texttt{gainL\_real} -- Real part of gain errors of the LCP receiver. Applied if \mbox{\textit{uvjones\_g\_on = True}}.
\item \texttt{gainL\_imag} -- Imaginary part of gain errors of the LCP receiver. Applied if \mbox{\textit{uvjones\_g\_on = True}}.
\item \texttt{leakR\_real} -- Real part of RCP leakage (`D-term'). Applied if \mbox{\textit{uvjones\_d\_on = True}}.
\item \texttt{leakR\_imag} -- Imaginary part of RCP leakage (`D-term'). Applied if \mbox{\textit{uvjones\_d\_on = True}}.
\item \texttt{leakL\_real} -- Real part of LCP leakage (`D-term'). Applied if \mbox{\textit{uvjones\_d\_on = True}}.
\item \texttt{leakL\_imag} -- Imaginary part of LCP leakage (`D-term'). Applied if \mbox{\textit{uvjones\_d\_on = True}}.
\item \texttt{feed\_angle} -- Feed rotation angle offsets.
\item \texttt{mount} -- Mount type. Most VLBI stations have an alt-azimuth mount.
\item \texttt{dish\_diameter} -- The total diameter of the telescope dish. The effective diameter is obtained after multiplication with \texttt{ap\_eff}.
\item \texttt{xyz\_position\_m} -- The x,y,z position of the telescope on Earth in meters.
\end{enumerate}
An \href{https://bitbucket.org/M\_Janssen/symba/src/master/symba\_input/VLBIarrays/eht.antennas}{example} of an antenna table is given in the source code.

\item \textit{rpicard\_refants} -- The prioritized list of reference station for the global fringe-fit used to calibrate the data.

\item \textit{rpicard\_solints} -- The range of possible solution intervals for the global fringe-fit used to calibrate the data. Should be set to the range of atmospheric coherence times.

\item \textit{vexfile} -- If \textit{obs\_vex = True}, a vex file can be given with an observing schedule to be followed. All scans of a specified \textit{vex\_source} will be observed.
An \href{https://bitbucket.org/M_Janssen/symba/src/master/symba_input/vex_examples/EHT2017/e17e11.vex}{example} of a vex file is given in the source code.
If \mbox{\textit{obs\_vex = False}}, The \textit{ms\_StartTime}, \textit{ms\_obslength}, \textit{ms\_nscan}, and \textit{ms\_scan\_lag} parameters can be used to define a custom observing schedule for a source at \textit{ms\_RA} and \textit{ms\_DEC} coordinates.

\item \textit{input\_fitsimage} -- The input sky model. Next to FITS images, several types of txt based models are supported. Stokes I, Q, U,V information can be provided for polarimetric observations (\mbox{\textit{input\_fitspol = True}}). Consecutively numbered files can be used to observe a movie of a time-variable source. The duration of single movie frame should be specified with the \textit{frameduration} parameter.
An \href{https://bitbucket.org/M_Janssen/symba/src/master/symba_input/fits_examples/}{example} of a FITS image is given in the source code.
The observing frequency is set by the \textit{skyfreq} parameter, unless a FITS input model is used; then it is read from the FITS header and the \textit{skyfreq} value is ignored.

\item \textit{realdata\_uvfits} -- If \textit{match\_uv = True}, the uv-coverage of the simulated data will be cropped-down to contain only data points that are also present in a specified UVFITS file. An \href{https://bitbucket.org/M_Janssen/symba/src/master/symba_input/uvfits/}{example} of a UVFITS file from a real observation is given in the source code.

\item \textit{ms\_dnu} -- The bandwidth of the simulated observation.

\item \textit{ms\_nchan} -- The number of frequency channels in the simulated observation.

\item \textit{trop\_*} -- Parameters controlling the atmospheric influences on the simulated data.

\end{itemize}

\section{Output}
\label{sec:output}

The pipeline will use \href{https://github.com/rdeane/MeqSilhouette}{\mbox{\textsc{MeqSilhouette}}} to create a synthetic data set in the MeasurementSet format with corruptions specified in the configuration file.
Depending on the other parameters set, the data generated goes through different calibration steps. Afterwards, the MeasurementSet data is averaged in frequency and, if \textit{time\_avg} is set, also in time. The data is then also exported in UVFITS format. Optionally, an image reconstruction of the data is made.


\section{Usage Examples}
\label{sec:usage}

\begin{enumerate}
\item The easiest usage example is to try one of the `emulate observations' scripts described in the README.
\item After installation, it makes sense to set default values for the systemwide parameters in the master\_input.txt file: \textit{rpicard\_path} and \textit{meqsilhouette\_path} should point to the correct folders on the host machine.
For \textit{ms\_antenna\_table}, \textit{vexfile}, \textit{input\_fitsimage}, and \textit{realdata\_uvfits}, \textit{/usr/local/src/symba/} should be changed to the path where \pipe{} is installed. It also makes sense to set \textit{outdirname = output}, because the best practice is to have a local copy of master\_input.txt in your working directory next to the output of every synthetic data generation run.
\item Assuming systemwide parameters have been set as described above, the typical steps when running the pipeline are:
\begin{enumerate}
\item \$ cp /path/to/symba/dir/master\_input.txt /path/to/working/dir/
\item \$ cd /path/to/working/dir/
\item \$ mkdir input
\item Copy over an antenna table, sky model, and vexfile observing schedule (or define a custom schedule in master\_input.txt).
Examples can be found in \href{https://bitbucket.org/M_Janssen/symba/src/master/symba_input/}{symba\_input/} in the repository:
\begin{enumerate}
\item \$ cp /path/to/symba/dir/symba\_input/VLBIarrays/eht.antennas input/antenna.params
\item \$ cp /path/to/symba/dir/symba\_input/fits\_examples/000000.fits input/000000.fits
\item \$ cp /path/to/symba/dir/symba\_input/vex\_examples/EHT2017/e17e11.vex input/sched.vex
\item Further customize the antenna table and vexfile if needed.
\end{enumerate}
\item Set the parameters in the master\_input.txt file: \textit{match\_uv = False}, when you do not want to exactly reproduce the UV-coverage of an existing observation. \textit{ms\_antenna\_table = input/antenna.params}, \textit{vexfile = input/sched.vex}, and \textit{input\_fitsimage = input/000000.fits}. Change other parameters where needed; in most cases only \textit{vex\_source}, \textit{input\_fitspol}, and \textit{frameduration} have to be adjusted.
\item \$ symba master\_input.txt \\
\textgreater\;An output/ folder should be created in the working directory and progress of \pipe{} should be printed in the terminal: First from \mbox{\textsc{MeqSilhouette}} generating the data, then from \mbox{\textsc{rpicard}} calibrating the data, and finally from \mbox{\textsc{eht-imaging}} reconstructing an image from the calibrated data. If the example inputs from the repository have been used without modifications, the image eventually reconstructed by the pipeline should look similar to the one shown in \autoref{fig:run-example}.
\end{enumerate}
\end{enumerate}

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{run_output_example.pdf}
      \caption{Example image reconstruction from \pipe{} when using default parameters as described in the text.
              }
         \label{fig:run-example}
\end{figure}

\newpage
\section{Updating casacore data}
\label{sec:casacore}
If you have \href{https://github.com/casacore/casacore}{\mbox{\textsc{casacore}}} installed on your system (which you should have if you have installed \pipe{} from source), you will have to keep data directories up-to-date.
These are \mbox{/var/lib/casacore/data} and \mbox{/usr/share/casacore/data} on Linux and \mbox{/usr/local/share/casacore/data} on macOS.
General instructions on updating \textsc{casa} data are given \href{https://casaguides.nrao.edu/index.php/Fixing_out_of_date_TAI_UTC_tables_(missing_information_on_leap_seconds)}{here}.

I recommend to take your default \textsc{casa} version, ensure that its data is up-to-date, and replace \mbox{/usr/share/casacore/data} and \mbox{/var/lib/casacore/data} (or \mbox{/usr/local/share/casacore/data} on macOS)
with a symlink to\\\mbox{/path/to/your/default/casaversion/data}.
The \textsc{casa} version from \href{https://bitbucket.org/M_Janssen/picard}{\mbox{\textsc{Rpicard}}} (see its README) is continuously kept up-to-date on the server that it is hosted on.

Due to the way that the \texttt{singularity build} command works, you will also have to keep the \mbox{\textsc{casacore}} data up-to-date, when using \href{https://singularity.lbl.gov}{\mbox{\textsc{Singularity}}}.
The reason for this seems to be that the \mbox{\textsc{casacore}} data directories on your local system are read when they exist instead of the ones in the \href{https://www.docker.com}{\mbox{\textsc{Docker}}} image.
This issue may be fixed in newer versions of \mbox{\textsc{Singularity}}.

\end{document}
