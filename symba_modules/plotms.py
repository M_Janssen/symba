import sys
import symba_modules.input_reader as input_reader
from optparse import OptionParser
import casatools as casac

def main():
    plot(sys.argv[-1])

def plot(master_input_file):
    master_inp = input_reader.read_master_inp(master_input_file)
    sourcenames = [master_inp.vex_source]

    for sourcename in sourcenames:
        plotms(vis='%s/%s'%(master_inp.outdirname, input_reader.ms_name),
                xdatacolumn='corrected',
                ydatacolumn='corrected',
                xaxis='uvwave',
                yaxis='amp',
                correlation='LL, RR',
                avgchannel=master_inp.ms_nchan,
                avgtime='10',
                title='Calibrated',
                ylabel='Visibility amplitude (Jy)',
                plotfile='{0}/{1}_amp_cal_freqavg_10s.png'.format(master_inp.outdirname, sourcename),
                overwrite=True,
                highres=True,
                dpi=300,
                showgui=False,
                clearplots=True)

        plotms(vis='%s/%s'%(master_inp.outdirname, input_reader.ms_name),
                xdatacolumn='corrected',
                ydatacolumn='corrected',
                xaxis='uvwave',
                yaxis='phase',
                correlation='LL, RR',
                avgchannel=master_inp.ms_nchan,
                avgtime='10',
                title='Calibrated',
                ylabel='Visibility phase (deg)',
                plotfile='{0}/{1}_phase_cal_freqavg_10s.png'.format(master_inp.outdirname, sourcename),
                overwrite=True,
                highres=True,
                dpi=300,
                showgui=False,
                clearplots=True)

        plotms(vis='%s/%s'%(master_inp.outdirname, input_reader.ms_name),
                xdatacolumn='data',
                ydatacolumn='data',
                xaxis='uvwave',
                yaxis='amp',
                correlation='LL, RR',
                title='Raw data',
                ylabel='Visibility amplitude (Jy)',
                plotfile='{0}/{1}_amp_nocal.png'.format(master_inp.outdirname, sourcename),
                overwrite=True,
                highres=True,
                dpi=300,
                showgui=False,
                clearplots=True)

        plotms(vis='%s/%s'%(master_inp.outdirname, input_reader.ms_name),
                xdatacolumn='data',
                ydatacolumn='data',
                xaxis='uvwave',
                yaxis='phase',
                correlation='LL, RR',
                title='Raw data',
                ylabel='Visibility phase (deg)',
                plotfile='{0}/{1}_phase_nocal.png'.format(master_inp.outdirname, sourcename),
                overwrite=True,
                highres=True,
                dpi=300,
                showgui=False,
                clearplots=True)
            
if __name__=="__main__":
    main()
