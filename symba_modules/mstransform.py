import os
import sys
import shutil
import casatasks as tasks

msname = sys.argv[-1]
tmpvis = msname+'.tmp'
shutil.move(msname, tmpvis)
tasks.mstransform(vis=tmpvis, outputvis=msname, usewtspectrum=True, datacolumn='data')
if os.path.isdir(tmpvis):
    shutil.rmtree(tmpvis, ignore_errors=True)
