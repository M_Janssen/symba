#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Prepare input for the different parts of the pipeline.
"""
import os
import re
import sys
import json
import copy
import shutil
import ehtim
import datetime
import numpy as np
import pipe_modules.auxiliary as auxiliary
import symba_modules.input_reader as input_reader
import symba_modules.observe_vex_movie as observe_vex_movie
import symba_modules.observe_novex_movie as observe_novex_movie
from glob import glob
from astropy import units as u
from astropy.io import fits
from astropy.coordinates import Angle
from optparse import OptionParser


def main():
    usage       = "%prog inputfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    set_input_files(args[0])


def set_input_files(master_input_file):
    """
    Fetch default input files from code repositories.
    Adjust these input files according to the master_input.txt file.
    Puts the source coordinates from the vex file into the fits file(s).
    Put the image cellsize from the fits file(s) into the MeqSilhouette json input.
    """
    print('\n>> Setting input files...')
    _pwd                = os.getcwd()
    master_inp          = input_reader.read_master_inp(master_input_file)
    rpic_inp, mqsil_inp = get_repo_inputs(master_inp.rpicard_path, master_inp.meqsilhouette_path, master_inp.outdirname, _pwd)

    jsonfile            = mqsil_inp+'/eht230.json'

    set_rpicard_inp(rpic_inp, master_inp)

    msfolder = master_inp.outdirname+'/'+input_reader.ms_name
    if os.path.isdir(msfolder):
        shutil.rmtree(msfolder, ignore_errors=True)
    if os.path.isdir(msfolder+'.flagversions'):
        shutil.rmtree(msfolder+'.flagversions', ignore_errors=True)
    print('>> Done.')
    print('\n>> Creating observations for the fits files...')
    if master_inp.obs_vex == 'True':
        ra, dec = RADEC_from_vex(master_inp.vexfile, master_inp.vex_source)
        obs_vex = True
    else:
        ra, dec = float(master_inp.ms_RA), float(master_inp.ms_DEC)
        obs_vex = False
    fitsfiles = glob(master_inp.input_fitsimage)
    if not fitsfiles:
        raise ValueError('No input fits image found. Please verify input parameters.')
    freq_min = 99999999999999999999999999999999999
    freq_max = -1
    for fitsimage in fitsfiles:
        fits_cellsize, fits_skyfreq = exchange_fits_coords(master_inp, ra, dec, fitsimage)
        if fits_skyfreq < freq_min:
            freq_min = fits_skyfreq
        if fits_skyfreq > freq_max:
            freq_max = fits_skyfreq
    freq_central = (freq_min + freq_max)/2.
    if freq_max - freq_min > 0 and str(float(master_inp.ms_dnu)) != str(freq_max - freq_min):
        raise ValueError('For a frequency-resolved source, set the bandwidth equal to the source frequency range.')
    set_meqsil_inp(jsonfile, master_inp, fits_cellsize, freq_central, ra, dec)
    observe_vex_movie.observe_vex_movie(master_inp, jsonfile, ra, dec, obs_vex)
    print('>> Done.')


def obslen_from_vex(vexfile):
    times = []
    with open(vexfile, 'r') as f:
        for line in f:
            if 'start' in line and 'source' in line:
                pstart = line.split('=')[1]
                pstart = pstart.split(';')[0]
                pstart = pstart.strip()
                times.append(datetime.datetime.strptime(pstart, '%Yy%jd%Hh%Mm%Ss'))
    return (times[-1]-times[0]).total_seconds()/3600. + 1


def RADEC_from_vex(vexfile, sourcename):
    identifier = 'def ' + sourcename
    f          = open(vexfile, 'r')
    lines      = f.readlines()
    found      = False
    for i,l in enumerate(lines):
        if identifier.lower() in l.lower():
            found    = True
            deflines = lines[i:i+30]
    f.close()
    if not found:
        raise IOError(sourcename + ' not found in ' + vexfile)
    else:
        for l in deflines:
            if 'J2000' in l:
                thisline = l
                break
    radec  = thisline.strip('\n').split(';')
    ra     = radec[0].replace(' ', '').strip('ra=')
    dec    = radec[1].replace(' ', '').strip('dec=')
    radeg  = round(Angle(ra, unit=u.hour).degree, 2)
    decdeg = round(Angle(dec, unit=u.deg).degree, 2)
    return radeg, decdeg


def exchange_fits_coords(master_inp, radeg_vex, decdeg_vex, fitsfile):
    if fitsfile.endswith('txt'):
        skyfreq  = float(master_inp.skyfreq)
        cellsize = 3.e-6
        rahms    = Angle(radeg_vex, unit=u.deg).hms
        rahms    = [round(rhms, 4) for rhms in rahms]
        decdms   = Angle(decdeg_vex, unit=u.deg).dms
        decdms   = [round(ddms, 4) for ddms in decdms]
        if open(fitsfile, 'r').readlines()[0].startswith('!'):
            tmpfitsfile = auxiliary.unique_filename(fitsfile)
            difmapmod_to_meqsilmod(fitsfile, tmpfitsfile, rahms, decdms)
            os.remove(fitsfile)
            shutil.move(tmpfitsfile, fitsfile)
            return cellsize, skyfreq
        f     = open(fitsfile,'r')
        lines = f.readlines()
        shift = False
        pos0  = []
        f.close()
        f = open(fitsfile, 'w')
        for l in lines:
            if l[0] == '#':
                f.write(l)
            else:
                change_line = l.split()
                if shift:
                    change_line[1] = format(rahms[0]+float(change_line[1])-float(pos0[1]), '.16f')
                    change_line[2] = format(np.abs(rahms[1])+np.abs(float(change_line[2]))-np.abs(float(pos0[2])), '.16f')
                    change_line[3] = format(np.abs(rahms[2])+np.abs(float(change_line[3]))-np.abs(float(pos0[3])), '.16f')
                    change_line[4] = format(decdms[0]+float(change_line[4])-float(pos0[4]), '.16f')
                    change_line[5] = format(np.abs(decdms[1])+np.abs(float(change_line[5]))-np.abs(float(pos0[5])), '.16f')
                    change_line[6] = format(np.abs(decdms[2])+np.abs(float(change_line[6]))-np.abs(float(pos0[6])), '.16f')
                else:
                    pos0 = copy.deepcopy(change_line)
                    shift = True
                    change_line[1] = str(rahms[0])
                    change_line[2] = str(np.abs(rahms[1]))
                    change_line[3] = str(np.abs(rahms[2]))
                    change_line[4] = str(decdms[0])
                    change_line[5] = str(np.abs(decdms[1]))
                    change_line[6] = str(np.abs(decdms[2]))
                f.write(' '.join(change_line)+'\n')
    elif fitsfile.endswith('fits'):
        fitsdata, header = fits.getdata(fitsfile, header=True)
        header_Nfreq     = get_val_for_type_fits(header, 'FREQ')
        header_Nra       = get_val_for_type_fits(header, 'RA')
        header_Ndec      = get_val_for_type_fits(header, 'DEC')
        header_ra        = 'CRVAL'+header_Nra
        header_dec       = 'CRVAL'+header_Ndec
        header_radelt    = 'CDELT'+header_Nra
        header_decdelt   = 'CDELT'+header_Ndec
        try:
            skyfreq = header['FREQ'] * 1.e-9
        except KeyError:
            header_freq = 'CRVAL'+header_Nfreq
            skyfreq     = header[header_freq] * 1.e-9
        cellsize           = 0.5 * (abs(header[header_radelt]) + abs(header[header_decdelt]))
        header['OBSRA']    = radeg_vex
        header['OBSDEC']   = decdeg_vex
        header[header_ra]  = radeg_vex
        header[header_dec] = decdeg_vex
        fits.writeto(fitsfile, fitsdata, header, overwrite=True)
        convert_stokesPI_to_I(fitsfile)
        convert_Jyperpix_fits(fitsfile)
    else:
        raise IOError('fitsfile has unknown extension: ' + fitsfile)
    return cellsize, skyfreq


def get_val_for_type_fits(fitsheader, val):
    all_options = ['CTYPE1', 'CTYPE2', 'CTYPE3', 'CTYPE4', 'CTYPE5', 'CTYPE6', 'CTYPE7', 'CTYPE8', 'CTYPE9']
    for opt in all_options:
        try:
            if val in fitsheader[opt]:
                return opt[5]
            else:
                pass
        except KeyError:
            pass


def convert_Jyperpix_fits(fitsfile):
    fitsinjyperpix = ehtim.image.load_fits(fitsfile)
    fitsinjyperpix.save_fits(fitsfile)


def convert_stokesPI_to_I(fitsfile):
    fitsdata, header = fits.getdata(fitsfile, header=True)
    header_Nstokes   = get_val_for_type_fits(header, 'STOKES')
    if not header_Nstokes:
        return
    header_stokes    = 'CRVAL'+header_Nstokes
    if float(header[header_stokes]) == -9:
        header[header_stokes] = 1
        fits.writeto(fitsfile, fitsdata, header, overwrite=True)


def alter_line(filename, search_pattern, new_line, str_char = '#', illegal_string='--Katzenkratzbaum--', strict=True):
    f     = open(filename, 'r')
    lines = f.readlines()
    for i, l in enumerate(lines):
        if l[0] != str_char:
            if strict:
                if re.search(r'\b' + search_pattern + r'\b', l) and illegal_string not in l:
                    lines[i] = new_line
            else:
                if search_pattern in l and illegal_string not in l:
                    lines[i] = new_line
    f.close()
    f = open(filename, 'w')
    for l in lines:
        f.write(l)
    f.close()


def string_to_bool(_string):
    """
    Convert a string to a bool if it matches one of the specified boolean expressions.
    """
    if _string == 'None':
        _string = None
    elif _string == 'True':
        _string = True
    elif _string == 'False':
        _string = False
    return _string


def difmapmod_to_meqsilmod(infile, outfile, rahms, decdms):
    outf = open(outfile, 'w')
    cntr = 0
    with open(infile, 'r') as f:
        for line in f:
            if not line.startswith('!'):
                rline = line.split()
                rline = [v.rstrip('v') for v in rline]
                flux  = rline[0]
                rad   = float(rline[1])
                ang   = float(rline[2]) * np.pi/180.
                try:
                    emaj = float(rline[3]) * 1.e-3
                    emin = emaj * float(rline[4])
                    pa   = rline[5]
                except IndexError:
                    emaj = 0.00000000000000000001
                    emin = 0.00000000000000000001
                    pa   = '0.0'
                xpos = rad * np.sin(ang)
                ypos = rad * np.cos(ang)
                ra   = Angle(xpos, unit=u.mas).hms
                dec  = Angle(ypos, unit=u.mas).dms
                snam = 's{0}'.format(str(cntr))
                rah  = str(ra[0] + rahms[0])
                ram  = format(ra[1] + rahms[1], '.16f')
                ras  = format(ra[2] + rahms[2], '.16f')
                decd = str(dec[0] + decdms[0])
                decm = format(dec[1] + decdms[1], '.16f')
                decs = format(dec[2] + decdms[2], '.16f')
                emaj = str(emaj)
                emin = str(emin)
                wln  = '{0} {1} {2} {3} {4} {5} {6} {7} 0 0 {8} {9} {10}'.format(snam, rah, ram, ras, decd, decm, decs, flux,
                                                                                 emaj, emin, pa
                                                                                )
                wln  += '\n'
                cntr += 1
                outf.write(wln)
    outf.close()


def get_repo_inputs(rpicard_path, meqsil_path, workdir, current_dir):
    p_mqs = workdir + '/MeqSilhouette_json_input/'
    p_rpc = workdir + '/input/'
    if not os.path.isdir(p_mqs):
        os.makedirs(p_mqs)
    if not os.path.isdir(p_rpc):
        os.makedirs(p_rpc)
    os.system('cp ' + meqsil_path + '/eht230.json ' + p_mqs)
    os.system('cp -r ' + rpicard_path + '/*.inp ' + p_rpc)
    return p_rpc, p_mqs


def set_rpicard_inp(rpicard_inp, master_inp, no_weight_calib=False):
    array_inp   = rpicard_inp+'/array.inp'
    array_inp_f = rpicard_inp+'/array_finetune.inp'
    obs_inp     = rpicard_inp+'/observation.inp'
    const_inp   = rpicard_inp+'/constants.inp'
    alter_line(array_inp_f, 'parang', 'parang=False\n')
    alter_line(array_inp, 'refant', 'refant='+master_inp.rpicard_refants+'\n')
    alter_line(array_inp, 'array_type', 'array_type=meqsil\n')
    alter_line(array_inp, 'fringe_solint_optimize_search_cal', 'fringe_solint_optimize_search_cal='+master_inp.fringe_solint+'\n')
    alter_line(array_inp, 'fringe_solint_optimize_search_sci', 'fringe_solint_optimize_search_sci='+master_inp.fringe_solint+'\n')
    alter_line(obs_inp, 'ms_name', 'ms_name='+input_reader.ms_name+'\n')
    alter_line(obs_inp, 'workdir', 'workdir=$pwd\n')
    alter_line(obs_inp, 'science_target', 'science_target='+master_inp.vex_source+'\n')
    alter_line(obs_inp, 'calibrators_instrphase', 'calibrators_instrphase=None\n')
    alter_line(obs_inp, 'calibrators_bandpass', 'calibrators_bandpass=None\n')
    alter_line(obs_inp, 'calibrators_rldly', 'calibrators_rldly=None\n')
    alter_line(obs_inp, 'calibrators_dterms', 'calibrators_dterms=None\n')
    alter_line(obs_inp, 'calibrators_phaseref', 'calibrators_phaseref=None\n')
    if hasattr(master_inp, 'N_cores') and master_inp.N_cores=='2':
        alter_line(const_inp, 'MS_partitioning', 'MS_partitioning=fitsidi\n')
    else:
        alter_line(const_inp, 'MS_partitioning', 'MS_partitioning=MS_fitsidi_clean\n')
    if hasattr(master_inp, 'rpicard_mbss') and master_inp.rpicard_mbss=='True':
        alter_line(array_inp_f, 'atmo_selfcal', 'atmo_selfcal=both\n')
    elif hasattr(master_inp, 'rpicard_mbss') and master_inp.rpicard_mbss.lower()=='only':
        alter_line(array_inp_f, 'atmo_selfcal', 'atmo_selfcal=False\n')
    else:
        alter_line(array_inp_f, 'atmo_selfcal', 'atmo_selfcal=only\n')
    if hasattr(master_inp, 'rpicard_fringecuts') and float(master_inp.rpicard_fringecuts)>0:
        sncut = master_inp.rpicard_fringecuts
        sncut_s = str(0.9*float(sncut))
        alter_line(array_inp_f, 'fringe_minSNR_sb_instrumental', 'fringe_minSNR_sb_instrumental='+sncut+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_coher', 'fringe_minSNR_mb_coher='+sncut_s+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_short_cal', 'fringe_minSNR_mb_short_cal='+sncut+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_long_sci', 'fringe_minSNR_mb_long_sci='+sncut_s+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_short_sci', 'fringe_minSNR_mb_short_sci='+sncut+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_shortFFT', 'fringe_minSNR_mb_shortFFT='+sncut+'\n')
        alter_line(array_inp_f, 'fringe_minSNR_mb_reiterate', 'fringe_minSNR_mb_reiterate='+sncut_s+'\n')
        alter_line(const_inp, 'fringefit_mb_interpolate_over_flags', 'fringefit_mb_interpolate_over_flags=True')
    alter_line(const_inp, 'mpi_memory_safety', 'mpi_memory_safety=0\n')
    alter_line(const_inp, 'long_solint_no_mpi', 'long_solint_no_mpi=False\n')
    alter_line(const_inp, 'exportuvfits', 'exportuvfits=True\n')
    alter_line(const_inp, 'num_selected_scans', 'num_selected_scans=99\n')
    alter_line(const_inp, 'avg_final_time', 'avg_final_time='+master_inp.time_avg+'\n')
    if no_weight_calib:
        alter_line(array_inp_f, 'calibration_tables/tsys.t',
                   "calib_tsys = calibration_tables/tsys.t;linear;False;'';'';TSYS;time;amp;R,L;False;None;0;'';'';''\n"
                  )
    if hasattr(master_inp, 'flag_instructions') and os.path.isfile(master_inp.flag_instructions):
        flagf_in_workdir = rpicard_inp+'/flag_input_file.flag'
        shutil.copy(master_inp.flag_instructions, flagf_in_workdir)
        flagf_in_input = 'flagfile_name={0}\n'.format(flagf_in_workdir)
        alter_line(const_inp, 'flagfile_name', flagf_in_input)


def set_meqsil_inp(jsonfile, master_inp, _cellsize, _ms_nu, ra_coord, dec_coord):
    with open(jsonfile, "r") as jsonf:
        jsondata = json.load(jsonf)
    jsondata["im_cellsize"]                       = str(_cellsize)+'arcsec'
    jsondata["input_fitsimage_cellsize"]          = str(_cellsize)+'arcsec'
    jsondata["outdirname"]                        = master_inp.outdirname
    jsondata["ms_antenna_table"]                  = master_inp.outdirname + input_reader.workdir_antenna_info
    jsondata["station_info"]                      = master_inp.outdirname + input_reader.workdir_station_info
    jsondata["input_fitsimage"]                   = master_inp.input_fitsimage
    jsondata["input_fitspol"]                     = string_to_bool(master_inp.input_fitspol) * 1
    jsondata["input_changroups"]                  = int(master_inp.input_changroups)
    jsondata["ms_nu"]                             = round(float(_ms_nu), 6)
    jsondata["ms_dnu"]                            = float(master_inp.ms_dnu)
    jsondata["ms_nchan"]                          = float(master_inp.ms_nchan)
    jsondata["ms_RA"]                             = ra_coord
    jsondata["ms_DEC"]                            = dec_coord
    jsondata["ms_StartTime"]                      = master_inp.ms_StartTime
    jsondata["ms_obslength"]                      = float(master_inp.ms_obslength)
    jsondata["ms_nscan"]                          = float(master_inp.ms_nscan)
    jsondata["ms_scan_lag"]                       = float(master_inp.ms_scan_lag)
    jsondata["ms_tint"]                           = float(master_inp.ms_tint)
    jsondata["ms_makeplots"]                      = 0
    jsondata["predict_oversampling"]              = 8191
    jsondata["elevation_limit"]                   = float(master_inp.elevation_limit) * 0.017453
    jsondata["add_thermal_noise"]                 = string_to_bool(master_inp.add_thermal_noise) * 1
    jsondata["trop_enabled"]                      = string_to_bool(master_inp.trop_enabled) * 1
    jsondata["trop_noise"]                        = string_to_bool(master_inp.trop_enabled) * 1
    jsondata["trop_attenuate"]                    = string_to_bool(master_inp.trop_attenuate) * 1
    jsondata["trop_turbulence"]                   = string_to_bool(master_inp.trop_turbulence) * 1
    jsondata["trop_fixdelays"]                    = string_to_bool(master_inp.trop_mean_delay) * 1
    jsondata["trop_mean_delay"]                   = 0
    jsondata["trop_percentage_calibration_error"] = float(master_inp.trop_percentage_calibration_error)
    jsondata["trop_makeplots"]                    = 0
    #TODO: Should proably set pointing_enabled to 1 based on trop_enabled for the intra-scan seeing and wind variations
    jsondata["pointing_enabled"]                  = string_to_bool(master_inp.pointing_enabled) * 1
    jsondata["bandpass_enabled"]                  = string_to_bool(master_inp.bandpass_enabled) * 1
    jsondata["bandpass_table"]                    = master_inp.bandpass_txt
    jsondata["bandpass_freq_interp_order"]        = 1
    jsondata["quantization_efficiency"]           = float(master_inp.quantization_efficiency)
    jsondata["pointing_makeplots"]                = 0
    jsondata["uvjones_g_on"]                      = string_to_bool(master_inp.uvjones_g_on) * 1
    jsondata["uvjones_d_on"]                      = string_to_bool(master_inp.uvjones_d_on) * 1
    jsondata["parang_corrected"]                  = string_to_bool(master_inp.parang_corrected) * 1
    jsondata["predict_seed"]                      = int(master_inp.predict_seed)
    jsondata["ms_correctCASAoffset"]              = string_to_bool(master_inp.ms_correctCASAoffset) * 1


    with open(jsonfile, "w") as jsonf:
        json.dump(jsondata, jsonf, indent=0)


if __name__=="__main__":
    main()
