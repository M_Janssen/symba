#
# Copyright (C) 2019 The Event Horizon Telescope Collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import glob
import ehtim as eh
import numpy as np
import symba_modules.input_reader as input_reader


def save_obs_txt_stokesI(obs, fname):
    """Save the observation data in a text file. """
    outdata = obs.unpack(['time_utc', 't1', 't2', 'u', 'v', 'amp', 'phase', 'sigma'])
    head = (
        "SRC:"+obs.source+" DATE(MJD):"+str(obs.mjd)+" FREQ:"+str(round(obs.rf/1.e9,4))+"GHz\n" +
        "time(UTC)      T1     T2   U(lambda)        V(lambda)         " +
        "Iamp(Jy)    Iphase(d)   " +
        "Isigma(Jy)"
    )

    fmts = ("%011.8f  %6s %6s  %16.4f  %16.4f  %10.8f  %10.4f  %10.8f")
    np.savetxt(fname, outdata, header=head, fmt=fmts)


if len(sys.argv) == 1:
    print ("usage: "+sys.argv[0]+" inputfile")
    sys.exit()

master_inp  = input_reader.read_master_inp(sys.argv[1])
sourcenames = [master_inp.vex_source]
for sourcename in sourcenames:
    if master_inp.match_uv == 'True':
        if master_inp.do_netcal == 'True':
            in_name  = '%s/%s_calibrated_netcal_uvflagged.uvf'%(master_inp.outdirname, sourcename)
        else:
            in_name  = '%s/%s_calibrated_uvflagged.uvf'%(master_inp.outdirname, sourcename)
    elif master_inp.do_netcal == 'True':
        in_name  = '%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename)
    else:
        in_name  = '%s/%s_calibrated.uvf'%(master_inp.outdirname, sourcename)
    out_name = os.path.splitext(in_name)[0]+'.txt'
    this_obs = eh.obsdata.load_uvfits(in_name, polrep='stokes')
    save_obs_txt_stokesI(this_obs, out_name)
all_fits = glob.glob(master_inp.outdirname+'/*.fits')
for fits in all_fits:
    corresponding_uvf = fits[:-5]+'.uvfits'
    if os.path.isfile(corresponding_uvf):
        out_name = os.path.splitext(corresponding_uvf)[0]+'.txt'
        this_obs = eh.obsdata.load_uvfits(corresponding_uvf, polrep='stokes')
        save_obs_txt_stokesI(this_obs, out_name)
