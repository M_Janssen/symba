import numpy as np
import math
import datetime
import sys
import glob
import os
from natsort import natsorted

def main(antabfolder, statlist):
    antablist = glob.glob('%s/*.antab'%antabfolder)
    antablist = natsorted(antablist)

    antablines = [ [] for i in range(len(statlist)) ]

    for i in range(len(antablist)):
        this_antab = open(antablist[i], 'r').readlines()
        for j in range(len(this_antab)):
            line = this_antab[j]
            if 'GAIN' in line or line == '/\n':
                continue
            elif 'TSYS' in line:
                station = line.split()[1]
                statindex = statlist.index(station)
                continue 
            else:
                antablines[statindex].append(line)


    for antabf in antablist:
        os.remove(antabf)

    with open (antabfolder + '/antab_concat.antab', 'w') as f:

        # Gain group (flat gain curves for now)
        for k in range(len(statlist)):
            print('GAIN %s ALTAZ DPFU = 1.0 POLY = 1.0 /'%statlist[k], file=f)

        # Tsys group
        for k in range(len(statlist)):
            print("TSYS %s INDEX = 'R1', 'L1' /"%statlist[k], file=f)
            for i in range(len(antablines[k])):
                print(antablines[k][i][:-2], file=f)
            print('/', file=f)
    f.close()
    return 0
