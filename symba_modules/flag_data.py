import os
import sys
import symba_modules.input_reader as input_reader


def main():
    flag(sys.argv[-1])

def flag(master_input_file):
    master_inp = input_reader.read_master_inp(master_input_file)
    if hasattr(master_inp, 'flag_instructions') and os.path.isfile(master_inp.flag_instructions):
        vis   = master_inp.outdirname + '/' + input_reader.ms_name
        flagf = str(master_inp.flag_instructions)
        print('>> Applying flags from {0}'.format(flagf))
        flagdata(vis=vis, mode='list', autocorr=False, inpfile=flagf, datacolumn='DATA', flagbackup=False)

if __name__=="__main__":
    main()
