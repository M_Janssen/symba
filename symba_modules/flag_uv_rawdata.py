import numpy as np
import os
import sys
import glob
import symba_modules.input_reader as input_reader
import symba_modules.add_uvfits_IFinfo as add_uvfits_IFinfo
from symba_modules.flag_uv_data import flag_uv
from optparse import OptionParser
import inspect
import ehtim as eh
import pyfits


def main():
    usage       = "%prog inputfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    flag_uv_rawdata(args[0])

def flag_uv_rawdata(master_input_file):
    print ('>> Matching raw data coverage to real data...')
    master_inp = input_reader.read_master_inp(master_input_file)
    scriptpath = '/'.join(os.path.abspath(inspect.stack()[0][1]).split('/')[:-1])
    inputms = master_inp.outdirname + '/bhc_synthetic.MS'
    tmpdir = '%s/sepchan'%master_inp.outdirname
    os.system('mkdir %s'%tmpdir)

    # Get correct antenna names
    obs_datfile = master_inp.realdata_uvfits
    obs_dat = eh.obsdata.load_uvfits(obs_datfile, polrep='circ')
    good_annames = []
    for i in range(len(obs_dat.data)):
        ant1 = obs_dat.data['t1'][i]
        if ant1 not in good_annames:
            good_annames.append(ant1)
        ant2 = obs_dat.data['t2'][i]
        if ant2 not in good_annames:
            good_annames.append(ant2)

    vislist = []
    for chan in range(int(master_inp.ms_nchan)):
        # Convert MS to separate uvfits files
        uvf_infile = '%s/rawdata_chan%s.uvfits'%(tmpdir, chan)
        uvf_flagged = '%s/rawdata_chan%s_uvflagged.uvfits'%(tmpdir, chan)
        os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c %s/flag_uv_rawdata_casafuncs.py exportuvfits %s %s %s'%(scriptpath, inputms, uvf_infile, chan))

        # Correct station names in uvfits files
        uvfits = pyfits.open(uvf_infile, mode='update')
        ANdata = uvfits['AIPS AN'].data
        ANhead = uvfits['AIPS AN'].header
        for i, antname in enumerate(ANdata['ANNAME']):
          thisant = ''.join(char for char in antname if char.isalnum())
          if thisant in good_annames:
            goodant = thisant
          else:
            for realant in good_annames:
              if thisant.startswith(realant):
                goodant = realant
                break
          ANdata['ANNAME'][i] = goodant
        for i in range(len(uvfits)):
          if uvfits[i].name == 'AIPS AN':
            extension_iter = i
            break
        pyfits.update(uvf_infile, ANdata, header=ANhead, ext=extension_iter)
        uvfits.close()

        if chan == 0:
            # Get timestamps that need to be removed        
            todel = flag_uv(uvf_infile, obs_datfile, uvf_flagged)
            print(todel)
        else:
            # Remove timestamps for all channels
            obs =  eh.obsdata.load_uvfits(uvf_infile, polrep='circ')
            obs.data = np.delete(obs.data, todel)
            obs.save_uvfits(uvf_flagged)
            add_uvfits_IFinfo.add_uvfits_IFinfo(uvf_flagged)

        # Convert flagged uvfits to MS 
        ms_flagged = '%s/rawdata_chan%s_uvflagged.MS'%(tmpdir, chan)
        vislist.append(ms_flagged)
        os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c %s/flag_uv_rawdata_casafuncs.py importuvfits %s %s'%(scriptpath, uvf_flagged, ms_flagged))
        os.system('rm %s %s' %(uvf_infile, uvf_flagged))

    # Concatenate flagged MS
    vislist = glob.glob('%s/*.MS'%tmpdir)
    os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c %s/flag_uv_rawdata_casafuncs.py concat %s %s'%(scriptpath, tmpdir, tmpdir+'/concat_flagged.MS'))
    for vis in vislist:
        os.system('rm -r %s'%vis)
    os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c %s/flag_uv_rawdata_casafuncs.py mstransform %s %s'%(scriptpath, '%s/concat_flagged.MS'%tmpdir, '%s/bhc_synthetic_uvflagged.MS'%master_inp.outdirname))
    os.system('rm -r %s'%tmpdir)
    print ('>> Done.')
    
    return 0        

if __name__=="__main__":
    main()

