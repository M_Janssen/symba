import casatools as casac
import sys
import glob

if sys.argv[3] == 'exportuvfits':
    exportuvfits(vis=sys.argv[4], fitsfile=sys.argv[5], spw='0:%s'%sys.argv[6], datacolumn='data')

if sys.argv[3] == 'importuvfits':
    importuvfits(fitsfile=sys.argv[4], vis=sys.argv[5])

if sys.argv[3] == 'concat':
    concat(vis=glob.glob(sys.argv[4]+'/*.MS'), concatvis=sys.argv[5])

if sys.argv[3] == 'mstransform':
    mstransform(vis=sys.argv[4], outputvis=sys.argv[5], combinespws=True, datacolumn='data')


