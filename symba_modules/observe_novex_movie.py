"""
*DEPRECATED* (using observe_vex_movie with use_vex=False now.
"""
import numpy as np
import datetime
import json
import sys
import os
import shutil
import glob
import symba_modules.generate_antab as generate_antab
import symba_modules.input_reader as input_reader
import symba_modules.concatenate_antab as concatenate_antab
import symba_modules.run_meqsilhouette_vex as run_meqsilhouette_vex

def observe_novex_movie(master_inp, json_filename):

    movie = True
    bhc_obs_path = os.path.dirname(os.path.realpath(generate_antab.__file__))
    sourcename = master_inp.vex_source
    framedur = float(master_inp.frameduration) # Duration of movie frame in seconds

# Load template json file
    with open(json_filename, 'r') as f:
        json_template = json.load(f)
    f.close()

    cadence = json_template['ms_tint'] # Needed for generating antab table

    imlist = glob.glob(master_inp.input_fitsimage)
    if imlist[0].endswith('txt'):
        txtmodel = True
    else:
        txtmodel = False
    imlist.sort()

    if movie == True:
        imdir = json_template['input_fitsimage']
        nframes = len(imlist)

# Modify json file 
    json_dir = master_inp.outdirname + '/json_files_movie_tmp/'
    if os.path.isdir(json_dir):
        shutil.rmtree(json_dir, ignore_errors=True)
    os.mkdir(json_dir)
    newjson = json_template.copy()
    
    obsdur = float(master_inp.ms_obslength) + float(master_inp.ms_scan_lag)*(int(master_inp.ms_nscan)-1) # Total duration of observation including gaps

    if master_inp.pointing_enabled == 'True':
        newjson['pointing_time_per_mispoint'] = obsdur/float(master_inp.ms_nscan)*60 # New pointing offset for each scan

    if movie == True:
        scanfolder = master_inp.outdirname + '/movie_sources/%s'%(sourcename) # Folder needs to have name of source because MeqS will put the folder name as source name in the MS
        if not os.path.exists(scanfolder):
            os.makedirs(scanfolder)
        if txtmodel:
            oldfiles = glob.glob('%s/*.txt'%scanfolder)
        else:
            oldfiles = glob.glob('%s/*.fits'%scanfolder)
        for f in oldfiles:
            os.remove(f)

        # Check frame duration
        nmovie = len(np.unique([nf.split('.')[0].split('-')[0] for nf in imlist]))
        nobs = int(round(obsdur*3600/framedur, 0))
        nobs = max(nobs, 1)
        frames_scan = [x for x in range(0, nobs)]
        if nobs > nmovie:
            raise IOError('Not enough movie frames in input folder to complete observation')
        elif nobs < nmovie:
            print ('>> More movie frames in input folder than required for observation. Will use first %s frames only.'%nobs)
            imlist = imlist[:nobs]
        for frame in frames_scan:
            for im in imlist:
                if '%06d.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-model.fits'%(scanfolder, frame)) # Following new MeqS naming conventions
                elif '%04d.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-model.fits'%(scanfolder, frame))
                elif '%06d-I-model.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-I-model.fits'%(scanfolder, frame))
                elif '%06d-Q-model.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-Q-model.fits'%(scanfolder, frame))
                elif '%06d-U-model.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-U-model.fits'%(scanfolder, frame))
                elif '%06d-V-model.fits'%(frame) in im:
                    shutil.copy2(im, '%s/t%04d-V-model.fits'%(scanfolder, frame))
                elif '.txt' in im:
                    shutil.copy2(im, '%s/%s.txt'%(scanfolder, sourcename))

        if not os.listdir(scanfolder):
            raise IOError('No fits files were copied to ' + scanfolder)

        if txtmodel:
            newjson['input_fitsimage'] = glob.glob('%s/*.txt'%scanfolder)[0]
        else:
            newjson['input_fitsimage'] = scanfolder

# Export new json file 
    with open(json_dir + '/json_novex.json', 'w') as f:
        json.dump(newjson, f)
    f.close()

# Observe 
    run_meqsilhouette_vex.main(json_dir + '/json_novex.json', input_reader.ms_name[:-3])

# Generate antab table
    os.system('casa --nologger -c {0}/generate_listobs.py {1}/{2}'.format(bhc_obs_path, master_inp.outdirname, input_reader.ms_name))
    generate_antab.main('{0}/{1}.obs'.format(master_inp.outdirname, input_reader.ms_name[:-3]), sourcename, cadence, master_inp.outdirname)

#fix ms
    os.system('casa --nologger -c {0}/mstransform.py {1}/{2}'.format(bhc_obs_path, master_inp.outdirname, input_reader.ms_name))
    
# Cleanup
    if os.path.isdir(json_dir):
        shutil.rmtree(json_dir, ignore_errors=True)
    for listobsf in glob.glob(master_inp.outdirname+'/*.obs'):
        os.remove(listobsf)
    for npy_mqsilf in glob.glob(master_inp.outdirname+'/*.npy'):
        os.remove(npy_mqsilf)
