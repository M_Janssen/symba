#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Copy the DATA column of a MS into the MODEL_DATA of the MS created by SYMBA.
"""
import sys
import pipe_modules.auxiliary as auxiliary
import symba_modules.input_reader as input_reader
import casatasks as tasks
import casatools as casac


def main():
    cpmoddat(sys.argv[-1])


def cpmoddat(master_input_file):
    master_inp = input_reader.read_master_inp(master_input_file)
    if not auxiliary.is_set(master_inp, 'cpDATAtoMODEL'):
        return
    elif master_inp.cpDATAtoMODEL == 'False':
        return
    elif not auxiliary.isdir(master_inp.cpDATAtoMODEL):
        print('MS {0} not found. Cannot copy DATA to MODEL_DATA.'.format(master_inp.cpDATAtoMODEL))
        return
    mytb    = casac.table()
    datams  = master_inp.cpDATAtoMODEL
    modelms = master_inp.outdirname+'/'+input_reader.ms_name
    try:
        mytb.open(datams)
    except RuntimeError:
        print('{0} is not a valid MS. Cannot copy DATA to MODEL_DATA'.format(datams))
        return
    print('Copying DATA from {0} to MODEL_DATA of {1}. Errors from the ft task can be ignored.'.format(datams, modelms))
    DATA = mytb.getcol('DATA')
    mytb.close()
    try:
        tasks.ft(vis         = modelms,
                 field       = '',
                 spw         = '',
                 model       = '',
                 nterms      = 1,
                 complist    = '',
                 incremental = False,
                 usescratch  = True
                )
    except:
        pass
    mytb.open(modelms, nomodify=False)
    mytb.putcol('MODEL_DATA', DATA)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


if __name__=="__main__":
    main()
