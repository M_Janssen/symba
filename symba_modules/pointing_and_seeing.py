# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Hacks into MeqSilhouette to create long timescale pointing errors and short timescale seeing+wind errors.
"""
import re
import numpy as np


def get_avg_cohert(MeqSilhouette_station_table):
    avg_coher_time = 0
    counter        = 0
    with open(MeqSilhouette_station_table, 'r') as msst:
        for i, line in enumerate(msst):
            if i>0:
                lline = re.split(r'\s{2,}', line)
                avg_coher_time += float(lline[5])
                counter        += 1
            else:
                pass
    return round(avg_coher_time / counter, 2)


def grow_pointing_offsets(ptgoffsets, growfactor):
    grow = 1+abs(float(growfactor))
    return [grow*ptgoff for ptgoff in ptgoffsets]


def amploss_from_ptgoff(pointing_offsets, PB_FWHM230, obs_freq_GHz=230.):
    PB_FWHM = np.asarray(np.asarray(PB_FWHM230) * 230. / float(obs_freq_GHz))
    return np.exp(-8 * np.log(2)**2 * (pointing_offsets/PB_FWHM)**2)


def compute_mispointings(MeqSilhouette_station_table, ptg2gain=1):
    ptg_errors = []
    with open (MeqSilhouette_station_table, 'r') as msst:
        for i, line in enumerate(msst):
            if i>0:
                lline           = re.split(r'\s{2,}', line)
                this_ptg_rms    = float(lline[6]) * float(ptg2gain)
                this_PB_FWHM230 = float(lline[7])
                ptg_errors.append(this_ptg_rms * np.random.randn())
            else:
                pass
    return ptg_errors


def get_beamsizes(MeqSilhouette_station_table):
    beams = []
    with open (MeqSilhouette_station_table, 'r') as msst:
        for i, line in enumerate(msst):
            if i>0:
                lline           = re.split(r'\s{2,}', line)
                this_PB_FWHM230 = float(lline[7])
                beams.append(this_PB_FWHM230)
            else:
                pass
    return beams


def apply_mispointing_amploss(MeqSilhouette_station_table, ptg_errors, PB_FWHM230, obs_freq_GHz):
    applied_gains = ''
    ptg_errors    = amploss_from_ptgoff(ptg_errors, PB_FWHM230, obs_freq_GHz)
    f             = open(MeqSilhouette_station_table, 'r')
    lines         = f.readlines()
    for i, l in enumerate(lines[1:]):
        lline         = re.split(r'\s{2,}', l)
        gR           = complex(lline[10])
        gL           = complex(lline[12])
        lline[10]     = str(gR * float(ptg_errors[i]))[1:-1]
        lline[12]     = str(gL * float(ptg_errors[i]))[1:-1]
        lines[i+1]    = '    '.join(lline)
        applied_gains+= lline[0] + ':' + str(round(float(ptg_errors[i]), 5)) + ' '
    f.close()
    f = open(MeqSilhouette_station_table, 'w')
    for l in lines:
        f.write(l)
    f.close()
    return applied_gains
