#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Prepare minimal input in master_input.txt file when emulating observations in Docker container.
"""
import os
import sys
from optparse import OptionParser
import symba_modules.generate_input_python as generate_input

masterinp_f = '/usr/local/src/symba/master_input.txt'


def main():
    #TODO: different shell scripts in emulate_observations should call this script with e.g. --eht-m87-e17e11 or --eht-sgr-e17c07
    #and the appropriate vex files, uvfits files with real coverage. etc. should be set accordingly here
    usage       = "%prog inputfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    set_masterinp(args[0])


def set_masterinp(inputfits):
    """
    Prepares master_input.txt file in Docker for the observation of a single FITS model file.
    """
    fitsfilepath = os.path.realpath(inputfits)
    generate_input.alter_line(masterinp_f, 'input_fitsimage', 'input_fitsimage = {0}\n'.format(fitsfilepath))


if __name__=="__main__":
    main()
