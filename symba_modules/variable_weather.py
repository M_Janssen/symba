# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Changes the station info file based on given variable weather parameters
"""
import datetime
import pandas as pd

def interpolate_station_weather(station_weather, scan_center_datetime, dt_string):
    for i in range(len(station_weather)):
        this_time_datetime = datetime.datetime.strptime(station_weather['time'][i], dt_string)
        if scan_center_datetime < this_time_datetime:
            index1 = i
            index0 = i-1
            break
    interp_dif = (scan_center_datetime - datetime.datetime.strptime(station_weather['time'][index0], dt_string)).total_seconds()
    interp_segment = (datetime.datetime.strptime(station_weather['time'][index1], dt_string) - datetime.datetime.strptime(station_weather['time'][index0], dt_string)).total_seconds()
    interp_fraction = interp_dif/interp_segment
    station_weatherparams_0 = station_weather.iloc[index0].tolist()
    station_weatherparams_1 = station_weather.iloc[index1].tolist()
    station_weatherparams = station_weatherparams_0
    for i in range(2, len(station_weatherparams)):
        station_weatherparams[i] = station_weatherparams_0[i] + interp_fraction * (station_weatherparams_1[i] - station_weatherparams_0[i])

    return station_weatherparams

def modify_station_info(stationfile, station_weatherparams):
    station = station_weatherparams[1]
    with open(stationfile, 'r') as f:
        lines = f.readlines()
        for i in range(1,len(lines)):                
            if lines[i].split()[0] == station:
                line = lines[i].split()
                line[2] = str(round(station_weatherparams[2], 2)) # pwv
                line[3] = str(int(round(station_weatherparams[3], 0))) # tground
                line[4] = str(int(round(station_weatherparams[4], 0))) # pground
                line[5] = str(round(station_weatherparams[5], 2)) # tc
                lines[i] = '    '.join(line)
                if '\n' not in lines[i]:
                    lines[i] += '\n'
    f.close()
    with open(stationfile, 'w') as f: 
        for l in lines:
            f.write(l)
    f.close()

    return 0

def modify_weather_data(weatherfile, scan, stationfile, dt_string = 'UTC,%Y/%m/%d/%H:%M:%S.00'):
    weatherdata = pd.read_csv(weatherfile, sep=' ', header=0)
    scan_start_datetime = datetime.datetime.strptime(scan.start, dt_string)
    scan_center_datetime = scan_start_datetime + datetime.timedelta(hours=float(scan.dur)/2.)

    # Select or compute the appropriate weather parameters for this scan
    for station in scan.stations:
        station_weather = weatherdata.loc[weatherdata['station'] == station]
        station_weather = station_weather.reset_index(drop=True)
        if len(station_weather) > 1:

            # Check if scan time is outside weather data coverage, if so take first or last weather entry
            if datetime.datetime.strptime(station_weather['time'][0], dt_string) > scan_center_datetime:
                station_weatherparams = station_weather.iloc[0].tolist()
            elif datetime.datetime.strptime(station_weather['time'][len(station_weather['time'])-1], dt_string) < scan_center_datetime:
                station_weatherparams = station_weather.iloc[-1].tolist()

            # Otherwise interpolate
            else:
                station_weatherparams = interpolate_station_weather(station_weather, scan_center_datetime, dt_string)
        elif len(station_weather) == 1:
            station_weatherparams = station_weather.iloc[0].tolist()
        else:
            raise ValueError('Could not find variable weather data for station %s.\nPut at least one entry in the variable weather input file.'%station)

        # Modify the station info file parameters
        modify_station_info(stationfile, station_weatherparams)

    return 0
