#!/usr/bin/env python
#
# Copyright (C) 2018 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Read master input file for both python and CASA.
"""
import os
import re
from collections import namedtuple

workdir_stations_input = '/MeqSilhouette_stations_input'
workdir_antenna_info = workdir_stations_input + '/ant_table'
workdir_station_info = workdir_stations_input + '/st_info'
#ms_name must end with .MS
ms_name = 'bhc_synthetic.MS'


def proper_line(f, comment_char = '#'):
    """
    Returns only non-commented and non-blank lines from input file.
    """
    lline = None
    for l in f:
        lline = l.rstrip()
        if lline:
            if comment_char not in lline[0]:
                yield lline


def read_master_inp(input_file):
    kwargs = []
    args   = []
    with open(input_file, 'r') as f:
        for line in proper_line(f):
            if '=' in line:
                line = line.replace(' ','')
                line = line.split('#', 1)[0]
                line = line.split('=')
                kwargs.append(line[0])
                args.append(line[1])
    if 'input_changroups' not in kwargs:
        kwargs.append('input_changroups')
        args.append('1')
    if 'variable_weather' not in kwargs:
        kwargs.append('variable_weather')
        args.append('False')
    outdir_indx       = kwargs.index('outdirname')
    args[outdir_indx] = os.path.abspath(args[outdir_indx])
    inputs_obj        = namedtuple('master_input', kwargs)
    inputs_list       = inputs_obj(*args)
    return inputs_list


def station_namespos(MeqSilhouette_station_table):
    names     = []
    positions = []
    with open (MeqSilhouette_station_table, 'r') as msst:
        for i, line in enumerate(msst):
            if i>0:
                lline     = re.split(r'\s{2,}', line)
                this_name = lline[0]
                this_pos  = lline[-1].split(',')
                names.append(this_name)
                positions.append([float(pos) for pos in this_pos])
            else:
                pass
    return names, positions
