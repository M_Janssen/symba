import os
import sys
import json
import glob
import shutil
import random
import datetime
import numpy as np
from astropy import units as u
from astropy.coordinates import EarthLocation, AltAz, ICRS, Angle
import symba_modules.input_reader as input_reader
import symba_modules.generate_antab as generate_antab
import symba_modules.concatenate_antab as concatenate_antab
import symba_modules.pointing_and_seeing as pointing_and_seeing
import symba_modules.variable_weather as variable_weather
import symba_modules.run_meqsilhouette_vex as run_meqsilhouette_vex


def observe_vex_movie(master_inp, json_filename, src_ms_RA, src_ms_DEC, use_vex=True):

    bhc_obs_path = os.path.dirname(os.path.realpath(generate_antab.__file__))
    sourcenames = [master_inp.vex_source] # Sources in the vex file you want to observe. If you want to observe all sources in the vex file, set sourcenames to None
    framedur = float(master_inp.frameduration) # Duration of movie frame in seconds

    def vextime2jsontime(time):
        year = int(time[:4])
        doy = int(time[5:8])
        hr = int(time[9:11])
        mins = int(time[12:14])
        sec = int(time[15:17])
        date = datetime.datetime(year, 1, 1) + datetime.timedelta(days = doy - 1, hours = hr, minutes = mins, seconds = sec)
        date = date.strftime('UTC,%Y/%m/%d/%H:%M:%S.00')
        return date

    class Source:
        def __init__(self, name, ra, dec):
            self.name = name
            self.ra = float(ra[:2])*(360./24) + float(ra[3:5])/60*(360./24) + float(ra[6:-1])/3600*(360./24)
            dec = dec.replace("'", 'd').split('d')
            self.dec = float(dec[0]) + np.sign(float(dec[0]))*float(dec[1])/60 + np.sign(float(dec[0]))*float(dec[2][:-1])/3600

    class Scan:
        def __init__(self, number, sourcename, start, dur, stations, from_vex=True):
            self.number = number
            self.sourcename = sourcename
            if from_vex:
                self.start = vextime2jsontime(start)
            else:
                self.start = start
            self.dur = float(dur)/3600.
            starth = self.start.strip('UTC,').split('/')[-1].split(':')
            starth = float(starth[0]) + float(starth[1])/60. + float(starth[2])/3600.
            self.midh = starth + self.dur/2.
            self.stations = stations

# Load vex file
    if use_vex:
        fin = open(master_inp.vexfile,'r')
        txt = fin.readlines()
        fin.close()

# Make list of observed sources
        slist=[]
        copy=False
        for line in txt:
            if line.strip() == '$SOURCE;':
                copy = True
            elif line.strip() == '$FREQ;':
                copy = False
            elif copy == True:
                slist.append(line)

        sourcelist=[]
        for line in slist:
            if line.strip() == 'enddef;':
                sourcelist.append(Source(name, ra, dec))
            elif 'source_name' in line:
                name = line.split()[-1][:-1]
            elif 'ra = ' in line and line[0] != '*':
                ra = line.split()[2][:-1]
                dec = line.split()[5][:-1]

    # Take only sources you want to observe
        if sourcenames:
            newsourcelist = []
            for s in sourcelist:
                if s.name in sourcenames:
                    newsourcelist.append(s)
            sourcelist = newsourcelist
    else:
        pass

    if use_vex:
# Read scans
        slist=[]
        copy=False
        for line in txt:
            if line.strip() == '$SCHED;':
                copy = True
            elif copy == True:
                slist.append(line)

        scanlist=[]
        stationlist=[]
        for line in slist:
            if line.strip() == 'endscan;':
                if len(stationlist) > 1:
                    scanlist.append(Scan(number, sourcename, start, dur, stationlist))
                stationlist=[]
            elif 'scan No' in line:
                number = line.split()[-1][2:-1]
            elif 'source=' in line:
                sourcename = line.split('=')[-1].strip()[:-1]
                start = line.split('=')[1].split()[0][:-1]
            elif 'station=' in line:
                dur = line.split()[3]
                stationlist.append(line.split()[0].split('=')[1][:-1].upper())

# Take only scans of sources you want to observe
        if sourcenames:
            newscanlist = []
            for s in scanlist:
                if s.sourcename in sourcenames:
                    newscanlist.append(s)
            scanlist = newscanlist
    else:
# Build custom observing schedule based on MeqSilhoutte input
        startt0_str   = master_inp.ms_StartTime.split(',')[1]
        startt0       = datetime.datetime.strptime(startt0_str, '%Y/%m/%d/%H:%M:%S.00')
        this_startt0  = startt0
        scan_duration = float(master_inp.ms_obslength) / int(master_inp.ms_nscan)
        scanlist      = []
        for scan_counter, scan in enumerate(range(int(master_inp.ms_nscan))):
            stationlist     = []
            st_name, st_pos = input_reader.station_namespos(master_inp.ms_antenna_table)
            for station, xyz_antenna in zip(st_name, st_pos):
                this_elev = compute_elev(float(src_ms_RA), float(src_ms_DEC), xyz_antenna,
                                         this_startt0.strftime('%Y-%m-%d %H:%M:%S')
                                        )
                if this_elev > float(master_inp.elevation_limit):
                    stationlist.append(station)
            if len(stationlist)>1:
                scanlist.append(Scan(str(scan_counter), master_inp.vex_source, this_startt0.strftime('UTC,%Y/%m/%d/%H:%M:%S.00'),
                                str(scan_duration*3600.), stationlist, False)
                               )

            this_startt0 = this_startt0 + datetime.timedelta(hours = scan_duration + float(master_inp.ms_scan_lag))
    if not scanlist:
        raise ValueError('No scans found. Is the source visible for the antennas in the proposed observation schedule?')

# Load template json file
    with open(json_filename, 'r') as f:
        json_template = json.load(f)
    f.close()

    cadence = json_template['ms_tint'] # Needed for generating antab table

    imlist = glob.glob(master_inp.input_fitsimage)
    if imlist[0].endswith('txt'):
        txtmodel = True
    else:
        txtmodel = False

    imdir = json_template['input_fitsimage']
    if master_inp.input_fitspol == 'True' or int(master_inp.input_changroups) > 1:
        #count each frequency and I,Q,U,V model fitsfile only once
        nframes = len(np.unique([os.path.basename(nf).split('.')[0].split('-')[0] for nf in imlist]))
    else:
        nframes = len(imlist)

    station_info_f = master_inp.outdirname + '/' + input_reader.workdir_station_info
    primary_beams  = pointing_and_seeing.get_beamsizes(station_info_f)
    avg_coher_time = pointing_and_seeing.get_avg_cohert(station_info_f)
    observing_freq = json_template['ms_nu']

    if int(master_inp.predict_seed) != -1:
        random.seed(float(master_inp.predict_seed))
        np.random.seed(int(master_inp.predict_seed))

    f_modelf = open(master_inp.outdirname+'/frame_modelfluxes.txt', 'w')
    f_modelf.write('#scan-startime UTC-scan-mid-hour scan-number frameduration[s] inputframes\n')
    f_modelf.close()

    if master_inp.pointing_enabled == 'True':
        f_ptggains = open(master_inp.outdirname+'/scan_ptg.gains', 'w')
        f_ptggains.write('#scan-startime scan-number {stations:gains}\n')
        f_ptggains.close()

# Modify json file for each scan.
    json_dir = master_inp.outdirname + '/json_files_movie_tmp/'
    if os.path.isdir(json_dir):
        shutil.rmtree(json_dir, ignore_errors=True)
    os.mkdir(json_dir)
    #last_maxnum = 0
    ptgthresh  = 1. - 1./float(master_inp.Nscan_repoint)
    ptgmaxdeg  = int(float(master_inp.Nscan_repoint) * 1.7)
    ptgoffsets = pointing_and_seeing.compute_mispointings(station_info_f, master_inp.pointing_rms2offset)
    ptgcounter = 0
    for scan_counter, scan in enumerate(scanlist):
        if random.random()>ptgthresh or ptgcounter>ptgmaxdeg:
            print('\n>>>> Using new pointing offsets for scan #{0}\n'.format(str(scan_counter)))
            ptgoffsets = pointing_and_seeing.compute_mispointings(station_info_f, master_inp.pointing_rms2offset)
            ptgcounter = 0
        else:
            ptgoffsets = pointing_and_seeing.grow_pointing_offsets(ptgoffsets, master_inp.Nscan_pointing_grow)
            ptgcounter+= 1
        newjson = json_template.copy()
        #if use_vex:
        #    #Placeholder piece of code for future feature of observing sci+cal.
#!!TODO: make sure that the coordinates are treated and rounded properly (see generate_input_python.py module)
        #    for source in sourcelist:
        #        if source.name == scan.sourcename:
        #            #newjson['input_fitsimage'] += '%s/%s_%s.fits'%(source.name, source.name, vexname[-10:-4])
        #            newjson['ms_RA'] = source.ra
        #            newjson['ms_DEC'] = source.dec
        #        else:
        #            pass
        #else:
        #    pass
        newjson['ms_obslength'] = float(scan.dur)

        newjson['ms_nscan']     = 1
        newjson['ms_scan_lag']  = 0

        # If not the first scan, use calculated CASA time offset to modify the start time
        if master_inp.ms_correctCASAoffset == 'True' and scan_counter != 0:
            with open('%s/CASAtimeOffset.txt'%master_inp.outdirname, 'r') as f:
                CASAtimeOffset  = float(f.read().replace('\n', ''))
            scan_start = datetime.datetime.strptime(scan.start, 'UTC,%Y/%m/%d/%H:%M:%S.00')
            scan_start -= datetime.timedelta(seconds = CASAtimeOffset)
            scan.start = scan_start.strftime('UTC,%Y/%m/%d/%H:%M:%S.00')
            newjson['ms_correctCASAoffset'] = 0
        newjson['ms_StartTime'] = scan.start

        if master_inp.trop_turbulence == 'True':
            newjson['pointing_time_per_mispoint'] = round(avg_coher_time / 60., 3)
#        if master_inp.pointing_enabled == 'True':
#            newjson['pointing_time_per_mispoint'] = 1+float(scan.dur)*60 # New pointing offset for each scan

        # Assign movie frames to scan
        scanstart = scan.start.split('/')[-1].split(':')
        scanstart_day = scan.start.split('/')[-2]
        scanstart_first = scanlist[0].start.split('/')[-1].split(':')
        scanstart_first_day = scanlist[0].start.split('/')[-2]
        scanstart_sec_first = float(scanstart_first[0])*3600 + float(scanstart_first[1])*60 + float(scanstart_first[2])
        scanstart_sec = float(scanstart[0])*3600 + float(scanstart[1])*60 + float(scanstart[2])
        if scanstart_day == scanstart_first_day:
            scanstart_sec -= scanstart_sec_first
        else:
            scanstart_sec += (24*3600-scanstart_sec_first)
        startframe = int(round((scanstart_sec/framedur), 0))
        nframes_scan = max(1, int(round(scan.dur*3600/framedur, 0)))
        frames_scan = [x for x in range(startframe, startframe+nframes_scan)]
        f_modelf = open(master_inp.outdirname+'/frame_modelfluxes.txt', 'a')
        f_modelf.write('{0} {1} {2} {3} {4}\n'.format(str(scan.start), str(scan.midh), str(scan_counter), str(framedur),
                                                      str(frames_scan))
                      )
        f_modelf.close()

        if master_inp.loop_movie == 'True':

            # Loop movie if it ends before the end of the observation.
            for i in range(nframes_scan):
                frames_scan[i] = frames_scan[i]%nframes

        else:
            dobreak = False
            for i in range(nframes_scan):
                if frames_scan[i] > nframes:
                    print('\n\n\n\n --- RAN OUT OF FITS FILES --- \n\n\n\n')
                    dobreak = True
                    break
            if dobreak:
                break
            frames_scan = np.unique(frames_scan)
            #frames_max = max(1,int(float(scan.dur)*3600/framedur)) + last_maxnum
            #if frames_max > nframes:
            #    print('\n\n\n\n --- RAN OUT OF FITS FILES --- \n\n\n\n')
            #    break
            #frames_scan = np.arange(last_maxnum, frames_max)
            #last_maxnum = frames_max

            # Put movie frames in separate folder so they can be observed
            #imlist_scan = [master_inp.outdirname + '/%06d.fits'%(frame) for frame in frames_scan]

        scanfolder = master_inp.outdirname + '/movie_sources/%s'%(scan.sourcename) # Folder needs to have the name of the source because MeqS will put the folder name as source name in the MS
        if not os.path.exists(scanfolder):
            os.makedirs(scanfolder)

        if txtmodel:
            oldfiles = glob.glob('%s/*.txt'%scanfolder)
        else:
            oldfiles = glob.glob('%s/*.fits'%scanfolder)
        for f in oldfiles:
            os.remove(f)
        files_copied = 0
        for frame in frames_scan:
            if int(master_inp.input_changroups) == 1:
                for im in imlist:
                    if '%06d.fits'%(frame) in im or '%06d-0000.fits'%(frame) in im:
                        shutil.copy2(im, '%s/t%04d-model.fits'%(scanfolder, frame)) # Following new MeqS naming conventions
                        files_copied += 1
                    elif '%06d-I-model.fits'%(frame) in im or '%06d-0000-I-model.fits'%(frame) in im:
                        shutil.copy2(im, '%s/t%04d-I-model.fits'%(scanfolder, frame))
                        files_copied += 1
                    elif '%06d-Q-model.fits'%(frame) in im or '%06d-0000-Q-model.fits'%(frame) in im:
                        shutil.copy2(im, '%s/t%04d-Q-model.fits'%(scanfolder, frame))
                        files_copied += 1
                    elif '%06d-U-model.fits'%(frame) in im or '%06d-0000-U-model.fits'%(frame) in im:
                        shutil.copy2(im, '%s/t%04d-U-model.fits'%(scanfolder, frame))
                        files_copied += 1
                    elif '%06d-V-model.fits'%(frame) in im or '%06d-0000-V-model.fits'%(frame) in im:
                        shutil.copy2(im, '%s/t%04d-V-model.fits'%(scanfolder, frame))
                        files_copied += 1
                    elif '.txt' in im:
                        shutil.copy2(im, '%s/%s.txt'%(scanfolder, scan.sourcename))
                        files_copied += 1
            else:
                for changroup in range(int(master_inp.input_changroups)):
                    for im in imlist:
                        if '%06d-%04d.fits'%(frame, changroup) in im:
                            shutil.copy2(im, '%s/t%04d-%04d-model.fits'%(scanfolder, frame, changroup)) # Following new MeqS naming conventions
                            files_copied += 1
                        elif '%06d-%04d-I-model.fits'%(frame, changroup) in im:
                            shutil.copy2(im, '%s/t%04d-%04d-I-model.fits'%(scanfolder, frame, changroup))
                            files_copied += 1
                        elif '%06d-%04d-Q-model.fits'%(frame, changroup) in im:
                            shutil.copy2(im, '%s/t%04d-%04d-Q-model.fits'%(scanfolder, frame, changroup))
                            files_copied += 1
                        elif '%06d-%04d-U-model.fits'%(frame, changroup) in im:
                            shutil.copy2(im, '%s/t%04d-%04d-U-model.fits'%(scanfolder, frame, changroup))
                            files_copied += 1
                        elif '%06d-%04d-V-model.fits'%(frame, changroup) in im:
                            shutil.copy2(im, '%s/t%04d-%04d-V-model.fits'%(scanfolder, frame, changroup))
                            files_copied += 1
                        elif '.txt' in im:
                            raise IOError('Frequency-resolved txt models currently not supported')


        if not os.listdir(scanfolder):
            raise IOError('No files were copied to ' + scanfolder)
        #if len(frames_scan) > files_copied:
        #    print('Skipping scan ' + str(scan) + ' since there are not enough fits files.')
        #    continue       

        # If the movie ends and is restarted during the scan, the fits file names need to be permutated so that MeqS observes them in the right order
        if master_inp.loop_movie == 'True' and 0 in frames_scan and nframes-1 in frames_scan and nframes>1:
            for i in range(1, nframes_scan):
                if frames_scan[i]-frames_scan[i-1] < 0:
                    delta = frames_scan[i-1] - frames_scan[0] + 1
            frames_scan = frames_scan[::-1]
            for i in range(nframes_scan):
                for changroup in range(int(master_inp.input_changroups)):
                    if int(master_inp.input_changroups) == 1:
                        try:
                            os.rename('%s/t%04d-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes))
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-I-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-I-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes))
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-Q-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-Q-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes))
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-U-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-U-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes))
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-V-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-V-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes))
                        except OSError:
                            pass
                    else:
                        try:
                            os.rename('%s/t%04d-%04d-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes), changroup)
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-%04d-I-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-I-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes), changroup)
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-%04d-Q-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-Q-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes), changroup)
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-%04d-U-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-U-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes), changroup)
                        except OSError:
                            pass
                        try:
                            os.rename('%s/t%04d-%04d-V-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-V-model.fits'%(scanfolder, (frames_scan[i] + delta)%nframes), changroup)
                        except OSError:
                            pass

        # If the first frame of the scan is not the first movie frame, the fits files need to be renamed
        if 0 not in frames_scan:
            delta = frames_scan[0] 
            for i in range(nframes_scan):
                if int(master_inp.input_changroups) == 1:
                    try:
                        os.rename('%s/t%04d-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-model.fits'%(scanfolder, frames_scan[i] - delta))
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-I-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-I-model.fits'%(scanfolder, frames_scan[i] - delta))
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-Q-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-Q-model.fits'%(scanfolder, frames_scan[i] - delta))
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-U-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-U-model.fits'%(scanfolder, frames_scan[i] - delta))
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-V-model.fits'%(scanfolder, frames_scan[i]), '%s/t%04d-V-model.fits'%(scanfolder, frames_scan[i] - delta))
                    except OSError:
                        pass
                else:
                    try:
                        os.rename('%s/t%04d-%04d-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-model.fits'%(scanfolder, frames_scan[i] - delta), changroup)
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-%04d-I-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-I-model.fits'%(scanfolder, frames_scan[i] - delta), changroup)
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-%04d-Q-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-Q-model.fits'%(scanfolder, frames_scan[i] - delta), changroup)
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-%04d-U-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-U-model.fits'%(scanfolder, frames_scan[i] - delta), changroup)
                    except OSError:
                        pass
                    try:
                        os.rename('%s/t%04d-%04d-V-model.fits'%(scanfolder, frames_scan[i], changroup), '%s/t%04d-%04d-V-model.fits'%(scanfolder, frames_scan[i] - delta), changroup)
                    except OSError:
                        pass


        if txtmodel:
            newjson['input_fitsimage'] = glob.glob('%s/*.txt'%scanfolder)[0]
        else:
            newjson['input_fitsimage'] = scanfolder


        # Modify station info, antenna table, and bandpass table
        stationfolder = master_inp.outdirname + input_reader.workdir_stations_input + '/' + 'scan%s'%scan.number 
        if not os.path.exists(stationfolder):
            os.makedirs(stationfolder)
        os.system('cp -r %s %s'%(master_inp.outdirname + input_reader.workdir_antenna_info, stationfolder))
        os.system('cp -r %s %s'%(master_inp.outdirname + input_reader.workdir_station_info, stationfolder))
        stationfile = stationfolder + '/' + input_reader.workdir_station_info.split('/')[-1]

        # Set gains to account for pointing solution deterioration
        if master_inp.pointing_enabled == 'True':
            applied_ptg_gains = pointing_and_seeing.apply_mispointing_amploss(stationfile, ptgoffsets, primary_beams,
                                                                              observing_freq
                                                                             )
            f_ptggains = open(master_inp.outdirname+'/scan_ptg.gains', 'a')
            f_ptggains.write('{0} {1} {2}\n'.format(str(scan.start), str(scan_counter), applied_ptg_gains))
            f_ptggains.close()

        # Remove stations that are not observing during this scan from the station info file
        with open(stationfile, 'r') as f:
            lines = f.readlines()
            todel = []
            for i in range(1,len(lines)):                
                if lines[i].split()[0] not in scan.stations:
                    todel.append(i)
            lines = np.delete(lines, todel)
        f.close()
        with open(stationfile, 'w') as f: 
            f.writelines(lines)
        f.close()

        # Remove stations that are not observing during this scan from the antenna file
        antennatable = stationfolder + '/' + input_reader.workdir_antenna_info.split('/')[-1]
        delstring = ''
        for i in range(len(todel)):
            todel[i] -= 1
            delstring += (str(todel[i]) + '-')
        if delstring:
            os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c {0}/mod_anttable.py {1} {2}'.format(bhc_obs_path, antennatable, delstring[:-1]))

        # If observing with variable weather, modify the weather parameters in the station info file
        if master_inp.variable_weather == 'True':
            variable_weather.modify_weather_data(master_inp.variable_weatherfile, scan, stationfile)

        # Remove stations that are not observing during this scan from the bandpass file
        if master_inp.bandpass_enabled == 'True':
            os.system('cp -r %s %s'%(master_inp.bandpass_txt, stationfolder))
            bandpassfile = stationfolder + '/' + master_inp.bandpass_txt.split('/')[-1]
            with open(bandpassfile, 'r') as f:
                lines = f.readlines()
                todel = []
                for i in range(1,len(lines)):
                    if lines[i].split()[0] not in scan.stations:
                        todel.append(i)
                lines = np.delete(lines, todel)
            f.close()
            with open(bandpassfile, 'w') as f:
                f.writelines(lines)
            f.close()
            newjson['bandpass_table'] = bandpassfile

        newjson['station_info'] = stationfile
        newjson['ms_antenna_table'] = antennatable


        # Set new seed number for this scan (to avoid correlated noise across scans). Seed must be <2**32.
        newjson['predict_seed'] = int(random.random()*4294000000) + int(scan.number)

        # Export new json file for this scan
        with open(json_dir + '/json_%s.json'%scan.number, 'w') as f:
            json.dump(newjson, f)
        f.close()

        # Observe scan
        run_meqsilhouette_vex.main(json_dir + '/json_%s.json'%(scan.number), 'scan%s_%s'%(scan.number, scan.sourcename), master_inp, delstring, bhc_obs_path)

        # Generate antab table
        if master_inp.trop_attenuate == 'True' and master_inp.trop_enabled == 'True':
            os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c {0}/generate_listobs.py {1}/scan{2}_{3}.MS'.format(bhc_obs_path, master_inp.outdirname, scan.number, scan.sourcename))
            generate_antab.main('{0}/scan{1}_{2}.obs'.format(master_inp.outdirname, scan.number, scan.sourcename), scan.sourcename, cadence, master_inp.outdirname)

        # Keep raw MeqSilhouette output if desired
        if master_inp.keep_rawdata == 'True':
            print ("Keeping raw MeqSilhouette data...")
            rawfolder = '%s/raw_output/scan%04d'%(master_inp.outdirname, int(scan.number))
            if not os.path.exists(rawfolder):
                os.makedirs(rawfolder)
            tocopy = ['*.npy','scan%04d_%s.antab'%(int(scan.number), scan.sourcename), 'scan%04d_%s.MS'%(int(scan.number), scan.sourcename), 'scan%04d_%s.obs'%(int(scan.number), scan.sourcename), 'atm_output', 'json_files_movie_tmp', 'MeqSilhouette_json_input', 'movie_sources', 'plots', 'dterms_sim.txt']
            for fname in tocopy:
                if os.path.exists('%s/%s'%(master_inp.outdirname, fname)):
                    os.system('cp -r %s/%s %s'%(master_inp.outdirname, fname, rawfolder))
                    #ugly renaming so that rPicard only recognizes concatenated antab
                    if 'antab' in fname:
                        os.system('mv %s/%s %s/%s'%(rawfolder, fname, rawfolder, fname[:-1]))
                if 'npy' in fname:
                    os.system('cp -r %s/%s %s'%(master_inp.outdirname, fname, rawfolder))
        else:
            os.system('rm %s/*receiver_noise*.npy'%master_inp.outdirname)
            os.system('rm %s/*turb*.npy'%master_inp.outdirname)
            os.system('rm %s/*terms*.npy'%master_inp.outdirname)

    f_modelf.close()

    vislist  = master_inp.outdirname + '/*.MS'
    _ms_name = master_inp.outdirname + '/' + input_reader.ms_name
    os.system('PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba /usr/local/src/for_picard/casa/bin/casa --nologger -c {0}/concatenate.py {1} {2}'.format(bhc_obs_path, vislist, _ms_name))
# Concatenate antab tables
    if master_inp.trop_attenuate == 'True' and master_inp.trop_enabled == 'True':
        stationlist = []
        with open('%s'%master_inp.outdirname + input_reader.workdir_station_info, 'r') as f:
            lines = f.readlines()
            for i in range(1,len(lines)): 
                stationlist.append(lines[i].split()[0]) 
        concatenate_antab.main(master_inp.outdirname, stationlist)
    if os.path.isdir(json_dir):
        shutil.rmtree(json_dir, ignore_errors=True)
    for listobsf in glob.glob(master_inp.outdirname+'/*.obs'):
        os.remove(listobsf)
    for npy_mqsilf in glob.glob(master_inp.outdirname+'/*.npy'):
        os.remove(npy_mqsilf)


def compute_elev(ra_source, dec_source, xyz_antenna, time):
    ra_src_deg       = Angle(ra_source, unit=u.deg)
    dec_src_deg      = Angle(dec_source, unit=u.deg)

    source_position  = ICRS(ra=ra_src_deg, dec=dec_src_deg)
    antenna_position = EarthLocation(x=xyz_antenna[0]*u.m, y=xyz_antenna[1]*u.m, z=xyz_antenna[2]*u.m)
    altaz_system     = AltAz(location=antenna_position, obstime=time)
    trans_to_altaz   = source_position.transform_to(altaz_system)
    elevation        = trans_to_altaz.alt
    return elevation.degree
