"""
Wrapper around eht-imaging conensus script for 2017 EHT M87 data.
The default parameter values have been changed to fiducial parameters, using the known total flux density, and a slight larger field of view of 200uas.
Additionally, small changes were made to also process the new 2020 EHT sites.
"""
# This script is meant to be a "consensus script" for M87 imaging and for tests on synthetic data


import os
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

import sys
import matplotlib
matplotlib.use('agg')

import numpy as np
import ehtim as eh
import argparse
import re
import time
import symba_modules.input_reader as input_reader
import symba_modules.read_input_models as read_input_models

tstart = time.time()

master_inp = input_reader.read_master_inp(sys.argv[1])

sourcenames = [master_inp.vex_source]

maxgain = float(master_inp.gain_tol)

for sourcename in sourcenames:
    if master_inp.do_netcal == 'True':
        inputobs = '%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename)
    else:
        inputobs = '%s/%s_calibrated.uvf'%(master_inp.outdirname, sourcename)
    outfits = '%s/%s_fiducial_ehtim.fits'%(master_inp.outdirname, sourcename)
    if master_inp.do_gausscal=='True':
        zero_spacing_flux_density = read_input_models.get_total_flux_density(master_inp, return_medval=True)
    else:
        zero_spacing_flux_density = 0
    im_fovfactor = float(master_inp.fov_image) / 128.

#=============  SET PARAMETERS =============#
    parser = argparse.ArgumentParser(description="perform M87 parameter search")
    parser.add_argument('-i', '--infile',    default=inputobs, help="input UVFITS file")
    parser.add_argument('-i2', '--infile2',  default="", help="optional 2nd input file (different band) for imaging")
    parser.add_argument('-z', '--zbl',       default=zero_spacing_flux_density, type=float, help="total compact flux density (Jy); <=0 defaults to observed total flux density")
    parser.add_argument('-s', '--simple',    default=100, type=float)
    parser.add_argument('-t', '--tv',        default=1, type=float)
    parser.add_argument('-v', '--tv2',       default=1, type=float)
    parser.add_argument('-l', '--l1',        default=0, type=float)
    parser.add_argument('-f', '--flux',      default=1e4, type = float, help = 'weight of flux regularization hyperparameter')
    parser.add_argument('-p', '--prior',     default=40, type=float, help='width of gaussian prior in uas')
    parser.add_argument('--fovfactor',       default=im_fovfactor, type=float, help="scaling factor for the field of view")
    parser.add_argument('-e', '--syserr',    default=0.02, type=float, help="fractional scaling error for non-closing systematic noise")
    parser.add_argument('--LZgauss_flux',    default=zero_spacing_flux_density, type=float, help='gaussian flux density LZ calibration (Jy)')
    parser.add_argument('--LZgauss_size',    default=60., type=float, help='gaussian FWHM for LZ calibration (uas)')
    parser.add_argument('-o', '--outfile',   default=outfits,  help='output FITS image')
    parser.add_argument('-T', '--Threads',   default=-1, type=int, help='max number of threads (-1 disables multiprocessing)')
    parser.add_argument('--maskraduas',      default=-1, type=float, help='mask radius (uas); use -1 to disable')
    parser.add_argument('--solint',          default=0., type=float, help='solution interval for self calibration (seconds)')
    parser.add_argument('--stop',            default=1e-4, type=float, help='stopping criterion for the imager')
    parser.add_argument('-r','--reversetaper',default=5, type=float, help='maximum image resolution (uas); use -1 to disable')
    parser.add_argument('--npixfactor',      default=1, type=float, help='optional rescaling factor for number of pixels')
    parser.add_argument('--snrcut',          default=0.0, type=float, help='Minimum acceptable snr for visibilities')
    parser.add_argument('--snrcutamp',       default=0.0, type=float, help='Minimum acceptable snr for amplitudes')
    parser.add_argument('--snrcutcphase',    default=0.0, type=float, help='Minimum acceptable snr for closure phase')
    parser.add_argument('--snrcutlogcamp',   default=0.0, type=float, help='Minimum acceptable snr for log closure amplitude')
    parser.add_argument('--snrcutcamp',      default=0.0, type=float, help='Minimum acceptable snr for closure amplitude')
    parser.add_argument('--major',           default=3, type=int, help='number of major calibration cycles (with blurring)')
    parser.add_argument('--ttype',           default='fast', help="Non-uniform fourier transform type ('direct', 'fast', or 'nfft')")
    parser.add_argument('--scanavg',         default=True, help='scan average data before imaging',action='store_true')
    parser.add_argument('--flagzbl',         default=False, help='Flag zero baselines?',action='store_true')
    parser.add_argument('--push',            default=True, help="option to push harder toward data fitting",action='store_true')
    parser.add_argument('--savepdf',         default=True, help='save image pdf (True or False)?',action='store_true')
    parser.add_argument('--imgsum',          default=True, help='generate image summary pdf',action='store_true')

    args, _ = parser.parse_known_args()

    argdict = {'zbl': args.zbl, 
               'l1':args.l1, 
               'simple': args.simple, 
               'tv':args.tv,
               'tv2':args.tv2,
               'flux':args.flux,
               'fovfactor':args.fovfactor,
               'syserr':args.syserr,
               'prior':args.prior*eh.RADPERUAS,
               'LZgauss_flux':args.LZgauss_flux,
               'LZgauss_size_uas':args.LZgauss_size,
               'reverse_taper_uas':args.reversetaper
              }
    zbl       = args.zbl
    reverse_taper_uas = args.reversetaper
    solint = args.solint
    clipfloor = -1.0
    if args.maskraduas > 0.:
        clipfloor = 0.0

    reg_term  = {'simple': args.simple,
                 'tv'    : args.tv,
                 'tv2'   : args.tv2,
                 'l1'    : args.l1,
                 'flux'  : args.flux}

## These are options to force single-thread processing (useful for parallel imaging runs)
#os.environ["NUMEXPR_NUM_THREADS"] = str(args.Threads)
#os.environ["OMP_NUM_THREADS"]     = str(args.Threads)

    fovfactor = args.fovfactor
    outfile   = args.outfile

# Additional parameters
    obsfile   = args.infile        # Pre-processed observation  file
    ttype     = args.ttype             # Type of Fourier transform ('direct', 'nfft', or 'fast')
    processes = args.Threads       # Number of parallel processes for self-cal
    sys_noise = argdict['syserr']  # Systematic noise added to complex visibilities

# Fixed parameters
    npix       = int(fovfactor*64*args.npixfactor)          # number of pixels across the reconstructed image
    fov        = fovfactor*128*eh.RADPERUAS # field of view of the reconstructed image
    prior_fwhm = argdict['prior']           # Gaussian prior size (fixed)
    transform  = 'log'                      # enforce positivity ('log') or not (None)
    maxit      = 100                        # Maximum number of convergence iterations for imaging
    gain_tol   = [0.02,maxgain]                 # asymmetric gain tolerance for self-cal
    min_gain_sites = ['LM','SP','AZ','PV']
#self_cal_sites = ['SM','JC','AA','AP','LM','SP','AZ'] # [] means to allow self-calibration for all sites
    self_cal_sites = []

# Reported SEFD error budget (from Paper III)
    SEFD_error_budget = {'AA':0.10, 'AP':0.11, 'AZ':0.07, 'LM':0.22, 'PV':0.10, 'SM':0.15, 'JC':0.14, 'SP':0.07, 'PB': 0.10, 'AM':0.10, 'GL':0.10, 'KP':0.10}

# Systematic noise tolerance for amplitude a-priori calibration errors
# Start with the SEFD noise (but need sqrt), then rescale to ensure that final results respect the stated error budget
    systematic_noise = SEFD_error_budget.copy()
    for key in systematic_noise.keys():
        systematic_noise[key] = ((1.0+systematic_noise[key])**0.5 - 1.0) * 0.25
# Add extra noise for the LMT, which has much more variability than the a-priori error budget
    systematic_noise['LM'] += 0.15

# Define the output file
    tag = outfile

# Define the initial regularization weights
    data_term = {'amp':.2, 'cphase':1, 'logcamp':1}

# Define snrcuts
    snrcut = {'vis':args.snrcut, 'amp':args.snrcutamp, 'cphase':args.snrcutcphase, 'logcamp':args.snrcutlogcamp, 'camp':args.snrcutcamp, 'bs':args.snrcutcphase}

#=============  PREPARE DATA =============#

# load the uvfits file
    if args.infile2 == '':
        obs = eh.obsdata.load_uvfits(obsfile)
        if args.scanavg:
            obs.add_scans()
            obs = obs.avg_coherent(0.,scan_avg=True)
    else:
        obs1 = eh.obsdata.load_uvfits(obsfile)
        obs2 = eh.obsdata.load_uvfits(args.infile2)
        if args.scanavg:
            obs1.add_scans()
            obs2.add_scans()
            obs1 = obs1.avg_coherent(0.,scan_avg=True)
            obs2 = obs2.avg_coherent(0.,scan_avg=True)
        # Add a slight offset to avoid mixed closure products
        obs2.data['time'] += 0.00002718
        obs = obs1.copy()
        obs.data = np.concatenate([obs1.data,obs2.data])

# Estimate the total flux density from any available zero-spacing baselines
    zbl_tot = 0
    try:
        zbl_tot = np.median(obs.unpack_bl('AA','AP','amp')['amp'])
    except IndexError:
        pass
    if not zbl_tot:
        try:
            zbl_tot = np.median(obs.unpack_bl('AA','AX','amp')['amp'])
        except IndexError:
            pass
    if not zbl_tot:
        try:
            zbl_tot = np.median(obs.unpack_bl('SM','JC','amp')['amp'])
        except IndexError:
            pass
    if zbl <= 0.0:
        zbl = zbl_tot
        if not zbl:
            zbl = 1
    elif zbl > zbl_tot:
        print('Warning: Specified total compact flux density exceeds total flux density measured on AA-AP!')

# Flag out sites in the tarr with no measurements
    allsites = set(obs.unpack(['t1'])['t1'])|set(obs.unpack(['t2'])['t2'])
    obs.tarr = obs.tarr[[o in allsites for o in obs.tarr['site']]]
    obs = eh.obsdata.Obsdata(obs.ra, obs.dec, obs.rf, obs.bw, obs.data, obs.tarr, source=obs.source, mjd=obs.mjd, ampcal=obs.ampcal, phasecal=obs.phasecal)

# Copy the original
#obs_orig = obs.copy()
    res = obs.res() # nominal array resolution, 1/longest baseline

    if zbl_tot and zbl != zbl_tot:
        # Rescale short baselines to excite contributions from extended flux. 
        # setting zbl < zbl_tot assumes there is an extended constant flux component of zbl_tot-zbl Jy 
        for j in range(len(obs.data)):
            if (obs.data['u'][j]**2 + obs.data['v'][j]**2)**0.5 < 0.1e9:
                for k in range(-8,0):
                    obs.data[j][k] *= zbl/zbl_tot

# Order stations. This is to create a minimal set of closure quantities with the highest snr
    obs.reorder_tarr_snr()

# From here on out, don't change obs. Use obs_sc to track gain changes.
    obs_sc = obs.copy()

    if args.flagzbl:
        obs_sc = obs_sc.flag_uvdist(uv_min=0.1e9)

# Make a Gaussian prior
    gaussprior = eh.image.make_square(obs, npix, fov).add_gauss(zbl, (prior_fwhm, prior_fwhm, 0, 0, 0))
    gaussprior = gaussprior.add_gauss(zbl*1e-3, (prior_fwhm, prior_fwhm, 0, prior_fwhm, prior_fwhm)) # to avoid gradient singularities

# Add a circular mask, if specified
    if args.maskraduas > 0.0:
        mask = eh.image.make_square(obs, npix, fov)
        mask = mask.add_tophat(1.0, args.maskraduas*eh.RADPERUAS)
        mask.imvec = (mask.imvec > 0)
        gaussprior.imvec *= mask.imvec
        gaussprior.imvec *= zbl/gaussprior.total_flux()
    else:
        mask = gaussprior.copy()
        mask.imvec = (mask.imvec * 0.0) + 1.0

    print("Mask Values:",np.min(mask.imvec),np.max(mask.imvec))

# Helper function to repeat imaging with blurring to assure good convergence
    def converge(major=args.major, blur_frac=1.0):
        for repeat in range(major):
            init = imgr.out_last().blur_circ(blur_frac*res)
            init.imvec *= mask.imvec
            imgr.init_next = init
            imgr.make_image_I(show_updates=False)

# Reverse taper
    if reverse_taper_uas > 0:
        obs_sc = obs_sc.reverse_taper(reverse_taper_uas*eh.RADPERUAS)

# Add non-closing systematic noise to the observation for imaging (reminder: this must be done *after* any averaging)
    obs_sc = obs_sc.add_fractional_noise(sys_noise)

# Save the initial data (before any self-cal but after the taper)
    obs_sc_init = obs_sc.copy()

# Self calibrate the LMT to a Gaussian model 
    if args.LZgauss_flux > 0.0 and args.LZgauss_size > 0:
        print("Self-calibrating the LMT to a Gaussian model for LMT-SMT...")
        # Need to start with data that have no reverse taper applied
        obs_LMT = obs_sc_init.flag_uvdist(uv_max=2e9)
        # Re-taper, if needed
        if reverse_taper_uas > 0:
            obs_LMT = obs_LMT.taper(reverse_taper_uas*eh.RADPERUAS)

        gausspriorLMT = eh.image.make_square(obs, npix, fov).add_gauss(args.LZgauss_flux, (args.LZgauss_size*eh.RADPERUAS, args.LZgauss_size*eh.RADPERUAS, 0, 0, 0))
        caltab = eh.selfcal(obs_LMT, gausspriorLMT, sites=['LM'], method='both', ttype=ttype, processes=processes, caltable=True, gain_tol=1.0, solution_interval=solint)
        #caltab = caltab.enforce_positive(min_gain = 0.95)
        #caltab = caltab.enforce_positive(sites=min_gain_sites, method='min', min_gain = 0.9)
        # Now apply the calibration solution to the full (and potentially tapered) dataset
        obs_sc = caltab.applycal(obs_sc, interp='nearest', extrapolate=True)

# First round of imaging
    print("Imaging with visibility amplitudes and closure quantities...")
    imgr = eh.imager.Imager(obs_sc, gaussprior, prior_im=gaussprior, flux=zbl, data_term=data_term, maxit=maxit, clipfloor=clipfloor, norm_reg=True, systematic_noise=systematic_noise, reg_term = reg_term, ttype=ttype, cp_uv_min=0.1e9, stop=args.stop, snrcut=snrcut)
    imgr.make_image_I(show_updates=False)
    converge()
#imgr.out_last().display(cbar_unit=['Tb'],label_type='scale',export_pdf='1.pdf')

# Self calibrate to the previous model (phase-only); solution_interval is 0 to align phases from hi/lo if needed
    obs_sc = eh.selfcal(obs_sc, imgr.out_last(), method='phase', ttype=ttype, solution_interval=0.0)

# Second round of imaging
    print("Imaging with visibilities and closure quantities...")
    init = imgr.out_last().blur_circ(res)
    init.imvec *= mask.imvec
    imgr = eh.imager.Imager(obs_sc, init, prior_im=gaussprior, flux=zbl, data_term={'vis':imgr.dat_terms_last()['amp']*10, 'cphase':imgr.dat_terms_last()['cphase']*10, 'logcamp':imgr.dat_terms_last()['logcamp']*10}, maxit=maxit, clipfloor=clipfloor, norm_reg=True, systematic_noise=systematic_noise, reg_term = reg_term, ttype=ttype, cp_uv_min=0.1e9, stop=args.stop, snrcut=snrcut)
    imgr.make_image_I(show_updates=False)
    converge()
#imgr.out_last().display(cbar_unit=['Tb'],label_type='scale',export_pdf='2.pdf')

# Self calibrate to the previous model starting from scratch (phase for all sites; amp for LMT)
    obs_sc = eh.selfcal(obs_sc_init, imgr.out_last(), method='phase', ttype=ttype, processes=processes, solution_interval=solint)
    caltab = eh.selfcal(obs_sc, imgr.out_last(), sites=['LM'], method='both', ttype=ttype, gain_tol=gain_tol, processes=processes, caltable=True, solution_interval=solint)
#caltab = caltab.enforce_positive(min_gain = 0.95)    
#caltab = caltab.enforce_positive(sites=min_gain_sites, method='min', min_gain = 0.9)
    obs_sc = caltab.applycal(obs_sc, interp='nearest',extrapolate=True)

    maxit = maxit

    if args.push:
        data_term_final = {'vis':imgr.dat_terms_last()['vis']*5, 'cphase':imgr.dat_terms_last()['cphase']*2, 'logcamp':imgr.dat_terms_last()['logcamp']*2} 
    else:
        data_term_final = {'vis':imgr.dat_terms_last()['vis'], 'cphase':imgr.dat_terms_last()['cphase'], 'logcamp':imgr.dat_terms_last()['logcamp']} 

#{'vis':imgr.dat_terms_last()['vis']*5, 'cphase':imgr.dat_terms_last()['cphase']*10, 'logcamp':imgr.dat_terms_last()['logcamp']*10}
    for repeat_selfcal in range(2):
        # Make an image -- now with complex visibilities; common systematic noise
        init = imgr.out_last().blur_circ(res)
        init.imvec *= mask.imvec
        imgr = eh.imager.Imager(obs_sc, init, prior_im=gaussprior, flux=zbl, data_term = data_term_final, maxit=maxit, clipfloor=clipfloor, norm_reg=True, systematic_noise=0.01, reg_term = reg_term, ttype=ttype, cp_uv_min=0.1e9, stop=args.stop, snrcut=snrcut)
        imgr.make_image_I(show_updates=False)
        converge()
        #imgr.out_last().display(cbar_unit=['Tb'],label_type='scale',export_pdf=str(repeat_selfcal+3) + '.pdf')

        # Self calibrate to the previous model (amplitude and phase)    
        #obs_sc = eh.selfcal(obs_sc, imgr.out_last(), sites=self_cal_sites, method='both', ttype=ttype, gain_tol=gain_tol, processes=processes, solution_interval=solint) 
        caltab = eh.selfcal(obs_sc_init, imgr.out_last(), sites=self_cal_sites, method='both', ttype=ttype, gain_tol=gain_tol, processes=processes, caltable=True, solution_interval=solint)        
        #caltab = caltab.enforce_positive(min_gain = 0.9)
        #caltab = caltab.enforce_positive(sites=min_gain_sites, method='min', min_gain = 0.9)
        obs_sc = caltab.applycal(obs_sc_init, interp='nearest',extrapolate=True)

    im_out = imgr.out_last().copy()

# Final self-cal (unrestricted)
    obs_sc = eh.selfcal(obs_sc, imgr.out_last(), sites=[], method='both', ttype=ttype, gain_tol=0.2, processes=processes, solution_interval=0.0) 

    if reverse_taper_uas > 0.0:
        im_out = im_out.blur_circ(reverse_taper_uas*eh.RADPERUAS)

# Save the results
    reg_dict_out   = {'flux':1,'cm':1,'l1':1,'simple':1,'tv':1,'tv2':1}
    chisq_dict_out = {'vis':1, 'amp':1,'cphase':1, 'logcamp':1, 'camp':1, 'bs':1}
    orig_chisq_dict_out =  {'vis':1, 'amp':1,'cphase':1, 'logcamp':1, 'camp':1, 'bs':1}
    orig_chisq_dict_out['vis']     = obs.chisq(im_out, dtype= 'vis',    cp_uv_min = 0.1e9, snrcut=snrcut['vis'], ttype=ttype)
    orig_chisq_dict_out['amp']     = obs.chisq(im_out, dtype= 'amp',    cp_uv_min = 0.1e9, snrcut=snrcut['amp'], ttype=ttype)
    orig_chisq_dict_out['cphase']  = obs.chisq(im_out, dtype= 'cphase', cp_uv_min = 0.1e9, snrcut=snrcut['cphase'], ttype=ttype)
    orig_chisq_dict_out['logcamp'] = obs.chisq(im_out, dtype= 'logcamp',cp_uv_min = 0.1e9, snrcut=snrcut['logcamp'], ttype=ttype)
    orig_chisq_dict_out['camp']    = obs.chisq(im_out, dtype= 'camp',   cp_uv_min = 0.1e9, snrcut=snrcut['camp'], ttype=ttype)
    orig_chisq_dict_out['bs']      = obs.chisq(im_out, dtype= 'bs',     cp_uv_min = 0.1e9, snrcut=snrcut['bs'], ttype=ttype)

    imgr.systematic_noise_next = 0.0
    imgr.dat_term_next = chisq_dict_out
    imgr.reg_term_next = reg_dict_out
    imgr.obs_next      = obs_sc
    imgr._change_imgr_params = True
    imgr.init_imager()
    chisqdict_final = imgr.make_chisq_dict(imgr.out_last().imvec[mask.imvec > 0])
    regdict_final   = imgr.make_reg_dict(imgr.out_last().imvec[mask.imvec > 0])
    tstop = time.time()

    outstr = ""
    for arg in sorted(argdict.keys()):
        print(arg)
        print(argdict[arg])
        outstr += '%s %0.2f\n' % (arg, argdict[arg])

    outstr += '##############################\n'
    outstr += '#uvfits: %s\n' % (obsfile) 
    outstr += '##############################\n'
    outstr += "time %f  \n" % (tstop - tstart)
    outstr += 'flux_out %0.2f\n' % im_out.total_flux()
    outstr += 'Chi squared relative to initial data:\n'
    for dataterm in sorted(orig_chisq_dict_out.keys()):
        outstr += 'chi^2_%s %0.10f\n'%(dataterm, orig_chisq_dict_out[dataterm])
    outstr += 'Chi squared relative to self-calibrated data:\n'
    for dataterm in sorted(chisqdict_final.keys()):
        outstr += 'chi^2_%s %0.10f\n'%(dataterm, chisqdict_final[dataterm])
    outstr += 'Regularization terms:\n'
    for regterm in sorted(regdict_final.keys()):
        outstr += 'S_%s %0.10f\n'%(regterm, regdict_final[regterm])

# Save the final image and summary
    im_out.save_fits(outfile)
    outtxt = os.path.splitext(outfile)[0] + '.txt'
    f = open(outtxt, 'w')
    f.write(outstr)
    f.close()
    if args.savepdf:
        im_out.display(cbar_unit=['Tb'],label_type='scale',export_pdf=os.path.splitext(outfile)[0] + '.pdf')
    if args.imgsum:
        obs_sc_filename = os.path.splitext(outfile)[0] + '.uvfits'
        #obs_uncal_filename = os.path.splitext(outfile)[0] + '_uncal.uvfits'
        #obs.save_uvfits(obs_uncal_filename)

        if reverse_taper_uas > 0:
            obs_sc = obs_sc.taper(reverse_taper_uas*eh.RADPERUAS)
        obs_sc.save_uvfits(obs_sc_filename)

        matplotlib.pyplot.close('all')
        #eh.imgsum(im_out, obs_sc, obs, os.path.splitext(outfile)[0] + '_imgsum.pdf',cp_uv_min=0.1e9,snrcut=snrcut)
