import glob
import numpy as np
import ehtim as eh
from pipe_modules.auxiliary import interpolate1d_single, natural_sort_Ned_Batchelder


def get_total_flux_density(master_inp, return_medval=False):
    imlist = glob.glob(master_inp.input_fitsimage)
    natural_sort_Ned_Batchelder(imlist)
    framedur_h = float(master_inp.frameduration) / 3600.
    fitsmodel  = True
    modelname  = []
    for im in imlist:
        if '-I-model.fits' in im:
            modelname.append(im)
            continue
        elif '-Q-model.fits' in im:
            continue
        elif '-U-model.fits' in im:
            continue
        elif '-V-model.fits' in im:
            continue
        elif '.fits' in im:
            modelname.append(im)
            continue
        elif '.txt' in im:
            modelname = [im]
            fitsmodel = False
            break
    if not modelname:
        raise ValueError('No model files found. How did you get this far?')
    if fitsmodel:
        if len(modelname)==1:
            modelim     = eh.image.load_fits(modelname[0])
            netcal_flux = modelim.total_flux()
        else:
            with open(master_inp.outdirname+'/frame_modelfluxes.txt', 'r') as modflx:
                for i, line in enumerate(modflx):
                    if i == 1:
                        start_h = line.strip('UTC,').split()[0].split('/')[-1].split(':')
                        start_h = float(start_h[0]) + float(start_h[1])/60. + float(start_h[2])/3600.
                    else:
                        pass
            # Interpolate fluxes with cyclic repetition until the end of day (ehtim netcal does not work across days).
            end_h = line.strip('UTC,').split()[0].split('/')[-1].split(':')
            end_h = float(end_h[0]) + float(end_h[1])/60. + float(end_h[2])/3600.
            if end_h < start_h:
                end_h = 24
            N_models = len(modelname)
            currentt = start_h
            mod_fs   = []
            mod_ts   = []
            counter  = 0
            while True:
                modelim = eh.image.load_fits(modelname[counter%N_models])
                mod_fs.append(modelim.total_flux())
                mod_ts.append(currentt)
                currentt += framedur_h
                if currentt > end_h:
                    break
                counter += 1
                if counter > 8640000000:
                    raise OverflowError('Stuck in loop while interpolating fluxes between models.')
            if return_medval:
                return np.median(mod_fs)
            netcal_flux = interpolate1d_single(mod_ts, mod_fs)
    else:
        netcal_flux = read_meqsil_txt_srcflux(modelname[0])
    return netcal_flux


def read_meqsil_txt_srcflux(txtfile):
    totalflux = 0
    with open(txtfile) as f:
        for line in f:
            if not line.startswith('#'):
                totalflux += float(line.split()[7])
    return totalflux
