import numpy as np
import math
import datetime
import sys

import glob
import os


def main(listobs, source, cadence, workdir):
    transmission_list = glob.glob(workdir + '/transmission_timestamp*')
    transmission = np.load(max(transmission_list, key=os.path.getctime))

    # Read data from listobs file
    fin = open(listobs, 'r')
    txt = fin.readlines()
    fin.close()

    startdatestring = 'startdate'
    onscan = False
    onstation = False
    stimes = []
    stations = []
    for line in txt:
        # Get date of observation
        if 'Observed from' in line:
            startdatestring = line.split()[2].split('/')[0]
            startdate = datetime.datetime.strptime(startdatestring, '%d-%b-%Y')
            continue

        # Read scan times
        if "OBSERVE_TARGET.ON_SOURCE" in line and source in line:
            onscan = True

        else:
            onscan = False

        if onscan == True:
            this_scan = []
            scanstart = line.split()[0].split('.')[0]
            if startdatestring in scanstart:
                scanstart = datetime.datetime.strptime(scanstart, '%d-%b-%Y/%H:%M:%S')
            else: 
                scanstart = scanstart.split(':')        
                scanstart = startdate + datetime.timedelta(hours=int(scanstart[0]), minutes=int(scanstart[1]), seconds=int(scanstart[2]))
            scanend = line.split()[2].split('.')[0].split(':')
            scanend = startdate + datetime.timedelta(hours=int(scanend[0]), minutes=int(scanend[1]), seconds=int(scanend[2]))
            pointsperscan = int((scanend-scanstart).total_seconds()/cadence) + 1
            for i in range(pointsperscan):
                this_scan.append(scanstart + datetime.timedelta(seconds=i*cadence))
            stimes.append(this_scan)

        # Read station names
        elif onstation == True:
            stations.append(line.split()[1])
        
        if 'Elevation' in line:
            onstation = True

    # Make antab file, reduce to one value per scan (transmission at scan start at center frequency)
    midchan = int(len(transmission[0,:,0])/2)-1

    with open ('%s.antab'%listobs[:-4], 'w') as f:

        # Gain group (flat gain curves for now)
        for k in range(len(stations)):
            print ('GAIN %s ALTAZ DPFU = 1.0 POLY = 1.0 /'%stations[k], file=f)

        # Tsys group
        for k in range(len(stations)):
            print ("TSYS %s INDEX = 'R1', 'L1' /"%stations[k], file=f)
            index = 0
            for i in range(len(stimes)):
                for j in range(len(stimes[i])):
                    index += 1
                    if j==0: # Get Tsys at beginning of scan
                        time = stimes[i][0]
                        antabtime = time.strftime('%j %H:%M.')
                        fracminutes = round(float(time.strftime('%S'))/60, 2)
                        antabtime += str(fracminutes).split('.')[1]
                        antabtsys = 1./transmission[index, midchan, k]
                        if math.isnan(antabtsys) == False:
                            print (str(antabtime)+' '+str(antabtsys)+' '+str(antabtsys), file=f)
            print ('/', file=f)
    
    return 0
