#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Prepare input for the different parts of the pipeline.
Must use CASA python environment.
"""
import os
import re
import sys
import shutil
import symba_modules.input_reader as input_reader
import casatools as casac


def main():
    set_input_antennas(sys.argv[-2], sys.argv[-1])


def set_input_antennas(filepath, master_input_file):
    """
    Prepares CASA antenna table and station info for MeqSilhouette from a single file.
    """
    if not os.path.isfile(master_input_file) or not master_input_file or 'generate_input_CASA.py' in master_input_file:
        return False
    print('\n>> Preparing stations...')
    antennas_template = os.path.dirname(os.path.realpath(filepath))+'/../symba_input/antenna_table.template'
    master_inp        = input_reader.read_master_inp(master_input_file)
    ant_table         = master_inp.outdirname + input_reader.workdir_antenna_info
    st_info           = master_inp.outdirname + input_reader.workdir_station_info
    if not os.path.isdir(ant_table):
        os.makedirs(ant_table)
    make_antenna_table(master_inp.ms_antenna_table, antennas_template, ant_table, st_info)
    print('>> Done.')


def make_antenna_table(input_antennas, skeleton_table, outtable_antenna, outtable_station):
    mytb = casac.table()
    if os.path.isdir(outtable_antenna):
        shutil.rmtree(outtable_antenna, ignore_errors=True)
    ants = []
    stf  = open(outtable_station, 'w')
    with open(input_antennas, 'r') as f:
        for line in f:
            if not line.rstrip('\n'):
                continue
            if 'PB_model' not in line:
                splitline = re.split(r'\s{2,}', line)
                ants.append((splitline[0], float(splitline[-2]), [float(pos) for pos in splitline[-1].split(',')]))
                stationline = line.rstrip(splitline[-1])
                stationline = stationline.rstrip()
                stationline = stationline.rstrip(splitline[-2])
                stationline = stationline.rstrip()
            else:
                stationline = 'station    T_rx[K]     pwv[mm]     gpress[mb]  gtemp[K]   c_time[sec]   ptg_rms[arcsec]   PB_FWHM230[arcsec]    PB_model    ap_eff    gainR_mean gainR_std  gainL_mean gainL_std  leakR_mean leakR_std  leakL_mean leakL_std   feed_angle[degree]  mount'
            if '\n' not in stationline:
                stationline += '\n'
            stf.write(stationline)
    stf.close()
    mytb.open(skeleton_table)
    mytb.copy(outtable_antenna, deep=True, valuecopy=True)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    mytb.open(outtable_antenna, nomodify=False)
    for _ in range(len(ants)-1):
        mytb.copyrows(outtable_antenna, nrow=1)
    for i,ant in enumerate(ants):
        mytb.putcell('NAME', i, ant[0])
        mytb.putcell('STATION', i, ant[0])
        mytb.putcell('DISH_DIAMETER', i, ant[1])
        mytb.putcell('POSITION', i, ant[2])
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


if __name__=="__main__":
    main()
