import os
import sys
import glob
from optparse import OptionParser
import symba_modules.input_reader as input_reader
import symba_modules.add_uvfits_IFinfo as add_uvfits_IFinfo
import symba_modules.read_input_models as read_input_models
import ehtim as eh

def main():
    usage       = "%prog inputfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    netcal(args[0])


def netcal(master_input_file):
    master_inp  = input_reader.read_master_inp(master_input_file)
    sourcenames = [master_inp.vex_source]
    gain_tol = float(master_inp.gain_tol)
    netcal_flux = read_input_models.get_total_flux_density(master_inp)
    if master_inp.time_avg=='False' or master_inp.time_avg=='None':
        solint = 10
    else:
        solint = master_inp.time_avg
    try:
        solint = float(solint)
    except ValueError:
        solint = float(solint[:-1])
    for sourcename in sourcenames:
        obs = eh.obsdata.load_uvfits('%s/%s_calibrated.uvf'%(master_inp.outdirname, sourcename), polrep='circ')
        #obs_nc = eh.network_cal(obs, zbl=float(master_inp.netcal_flux), sites=master_inp.netcal_stations.split(','), processes=3, scan_solutions=True, solution_interval = 0.0, method = 'amp')
        obs_nc = eh.network_cal.network_cal(obs, zbl=netcal_flux, scan_solutions=False,
                                            solution_interval=solint, method='amp', gain_tol=gain_tol,
                                            pol='RRLL'
                                           )
        try:
            obs_nc.save_uvfits('%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename))
        except Exception as e:
            if 'No such file or directory' in str(e):
                missf   = str(str(e).split(': u')[-1].strip().strip("\'"))
                pydir   = '/'.join(missf.split('/')[:-1])
                pyfiles = glob.glob(pydir+'/*')
                for f in pyfiles:
                    if not 'urlmap' in f:
                        os.symlink(f, missf)
                        break
                obs_nc.save_uvfits('%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename))
            else:
                print(e)
        add_uvfits_IFinfo.add_uvfits_IFinfo('%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename))


if __name__=="__main__":
    main()
