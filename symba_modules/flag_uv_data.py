import ehtim as eh
import numpy as np
import os
import sys
import glob
import symba_modules.input_reader as input_reader
import symba_modules.add_uvfits_IFinfo as add_uvfits_IFinfo
from optparse import OptionParser

def main():
    usage       = "%prog inputfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(args)<1:
        parser.print_help()
        sys.exit(1)
    flag_uv_caldata(args[0])

def flag_uv(obs_simfile, obs_datfile, outfile, time_tolerance = 6./3600):
    obs_dat = eh.obsdata.load_uvfits(obs_datfile, polrep='circ')
    obs_sim = eh.obsdata.load_uvfits(obs_simfile, polrep='circ')
    obs_uvflagged = obs_sim.copy()
    todel = []
    for i in range(len(obs_sim.data)):
        found_match = False
        tsim = obs_sim.data['time'][i]
        bl = [obs_sim.data['t1'][i], obs_sim.data['t2'][i]]
        for j in range(len(obs_dat.data)):
            tdat = obs_dat.data['time'][j]
            tdiff = np.abs(tsim-tdat)
            if tdiff < time_tolerance and obs_dat.data['t1'][j] in bl and obs_dat.data['t2'][j] in bl:
                found_match = True

            if found_match == True:
                break

        if found_match == True:
            continue
        else:
            todel.append(i)



    obs_uvflagged.data = np.delete(obs_uvflagged.data, todel)
    try:
        # Weird astropy error...
        obs_uvflagged.save_uvfits(outfile)
    except Exception as e:
        if 'No such file or directory' in str(e):
            missf   = str(str(e).split(': u')[-1].strip().strip("\'"))
            pydir   = '/'.join(missf.split('/')[:-1])
            pyfiles = glob.glob(pydir+'/*')
            for f in pyfiles:
                if not 'urlmap' in f:
                    os.symlink(f, missf)
                    break
            obs_uvflagged.save_uvfits(outfile)
        else:
            print(e)
    add_uvfits_IFinfo.add_uvfits_IFinfo(outfile)

    return todel

def flag_uv_caldata(master_input_file):

    print (">> Flagging calibrated uv points that are not in real data...")

    master_inp = input_reader.read_master_inp(master_input_file)

    sourcenames = [master_inp.vex_source]
    for sourcename in sourcenames:
        if master_inp.do_netcal == 'True':
            obs_simfile = '%s/%s_calibrated_netcal.uvf'%(master_inp.outdirname, sourcename)
            outfile = '%s/%s_calibrated_netcal_uvflagged.uvf'%(master_inp.outdirname, sourcename)
        else:
            obs_simfile = '%s/%s_calibrated.uvf'%(master_inp.outdirname, sourcename)
            outfile = '%s/%s_calibrated_uvflagged.uvf'%(master_inp.outdirname, sourcename)

        flag_uv(obs_simfile, master_inp.realdata_uvfits, outfile)
    print (">> Done.")

if __name__=="__main__":
    main()

