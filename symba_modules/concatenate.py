import casatasks as tasks
import glob
import sys
import os
import shutil
import casatools as casac

vislist_init = sys.argv[1:]
tmpvis       = sys.argv[-1]+'.concat'
vislist      = []
for vis in vislist_init:
    if vis!=tmpvis and vis!=sys.argv[-1]:
        vislist.append(vis)

if os.path.isdir(tmpvis):
    shutil.rmtree(tmpvis, ignore_errors=True)

tasks.concat(vis=vislist,concatvis=tmpvis,freqtol="",dirtol="",respectname=False,timesort=True,copypointing=True,
visweightscale=[],forcesingleephemfield="")

mytb = casac.table()
mytb.open(tmpvis, nomodify=False)
nrows = mytb.nrows()
for row in range(nrows):
    mytb.putcell('OBSERVATION_ID', row, 0)
mytb.flush()
mytb.done()
mytb.clearlocks()

starttimes = []
endtimes   = []
mytb.open(tmpvis+'/OBSERVATION', nomodify=False)
nrows = mytb.nrows()
for row in range(nrows):
    trange = mytb.getcell('TIME_RANGE', row)
    starttimes.append(trange[0])
    endtimes.append(trange[-1])
mytb.putcell('TIME_RANGE', row, [min(starttimes), max(endtimes)])
mytb.removerows(list(range(1,nrows)))
mytb.flush()
mytb.done()
mytb.clearlocks()

if os.path.isdir(sys.argv[-1]):
    shutil.rmtree(sys.argv[-1], ignore_errors=True)

if os.path.isdir(sys.argv[-1]+'.flagversions'):
    shutil.rmtree(sys.argv[-1]+'.flagversions', ignore_errors=True)

tasks.mstransform(vis=tmpvis, outputvis=sys.argv[-1], usewtspectrum=True, datacolumn='data')

if os.path.isdir(tmpvis):
    shutil.rmtree(tmpvis, ignore_errors=True)

for vis in vislist:
    if os.path.isdir(vis):
        shutil.rmtree(vis, ignore_errors=True)
