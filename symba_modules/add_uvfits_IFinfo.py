#Thanks to Cheng-Yu Kuo.
#It seems ehtim deltes the IF metadata required by AIPS in the UVFITS header.
from astropy.io import fits


def add_uvfits_IFinfo(uvf):
    uvfits_file = fits.open(uvf)

    try:
        _ = uvfits_file[0].header['CTYPE5']
    except KeyError:
        uvfits_file[0].header.insert('CROTA4',('CTYPE5','IF'),after=True)
    try:
        _ = uvfits_file[0].header['CRVAL5']
    except KeyError:
        uvfits_file[0].header.insert('CTYPE5',('CRVAL5',1.0),after=True)
    try:
        _ = uvfits_file[0].header['CDELT5']
    except KeyError:
        uvfits_file[0].header.insert('CRVAL5',('CDELT5',1.0),after=True)
    try:
        _ = uvfits_file[0].header['CRPIX5']
    except KeyError:
        uvfits_file[0].header.insert('CDELT5',('CRPIX5',1.0),after=True)
    try:
        _ = uvfits_file[0].header['CROTA5']
    except KeyError:
        uvfits_file[0].header.insert('CRPIX5',('CROTA5',0.0),after=True)

    uvfits_file.writeto(uvf, overwrite=True)
    uvfits_file.close()
