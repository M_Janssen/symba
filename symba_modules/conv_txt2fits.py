import os
import sys
import h5py
import copy
import glob
import random
import shutil
import string
import astropy
import tempfile
import numpy as np
import ehtim as eh
from astropy.coordinates import SkyCoord
import symba_modules.input_reader as input_reader
from symba_modules.generate_input_python import alter_line, obslen_from_vex
from pipe_modules.auxiliary import natural_sort_Ned_Batchelder, is_set


def load_singlepol_im_hdf5(filename,sourcename='SgrA',plot=0,savefits=0,showplot=0,header='old'):

    """
       orginal file by George Wong
       modified by Christian M. Fromm

       Read in an image from an hdf5 file.
       Args:
            filename (str)  : path to input hdf5 file
            sourename (str) : name of source
       optional Args:
            plot (integer) : create a 1 or not 0
            savefits (integer) : store image as fits 1 or not 0
            showplot (integer) : if show plot 1 or not 0
       Returns:
            Dictionary including:
            image: loaded image object
            + GRRT and GRMHD parameters
    """

    ###Load information from hdf5 file
    hfp = h5py.File(filename)

    if header=='new':
        ###get main image parameters
        dsource = hfp['header']['dsource'][()]                  # distance to source in cm
        jyscale = hfp['header']['scale'][()]                    # convert cgs intensity -> Jy flux density
        rf = hfp['header']['freqcgs'][()]                       # in cgs
        tunit = hfp['header']['units']['T_unit'][()]            # in seconds
        lunit = hfp['header']['units']['L_unit'][()]            # in cm
        DX = hfp['header']['camera']['dx'][()]                  # in GM/c^2
        nx = hfp['header']['camera']['nx'][()]                  # width in pixels
        time = hfp['header']['t'][()]*tunit/3600.               # time in hours


        ###emission parameters
        rhigh=hfp['header']['emission']['Rhigh'][()]            # rhigh value for R-beta model
        rlow=hfp['header']['emission']['Rlow'][()]              # rlow value for R-beta model
        sigmacut=hfp['header']['emission']['sigmacut'][()]      # sigma cut used during GRRT (exclude regions with sigma>sigmacut
        betacrit=hfp['header']['emission']['betacrit'][()]      # critical beta value used during GRRT
        normflux=hfp['header']['emission']['normflux'][()]      # normalisation flux at 230GHz
        fov=hfp['header']['emission']['fov'][()]                # used field of view (-fov, fov)

        mbh=hfp['header']['bh']['mbh'][()]                      # mass of black hole
        spin=hfp['header']['bh']['spin'][()]                    # spin of black hole
        time=hfp['header']['bh']['time'][()]                    # time of the snapshot in M
        inc=hfp['header']['bh']['inc'][()]                      # viewing angle used during GRRT
        acc=hfp['header']['bh']['acc'][()]                      # accretion model of the used GRMHD

        ###get data
        unpoldat = np.copy(hfp['unpol'])                        # NX,NY
        hfp.close()

        ###Correct image orientation
        unpoldat = np.flip(unpoldat.transpose((1,0)),axis=0)


        ###--> use astropy to get source location
        #get source postion
        loc=SkyCoord.from_name(sourcename)

        #get RA and DEC in degree
        ra=loc.ra.deg
        dec=loc.dec.deg

        print ('Source: %s located a RA: %s DEC: %s' %(sourcename,str(loc.ra), str(loc.dec)))

    if header=='old':


        ###get main image parameters
        dsource = hfp['header']['dsource'][()]                  # distance to source in cm
        jyscale = hfp['header']['scale'][()]                    # convert cgs intensity -> Jy flux density
        rf = hfp['header']['freqcgs'][()]                       # in cgs
        tunit = hfp['header']['units']['T_unit'][()]            # in seconds
        lunit = hfp['header']['units']['L_unit'][()]            # in cm
        DX = hfp['header']['camera']['dx'][()]                  # in GM/c^2
        nx = hfp['header']['camera']['nx'][()]                  # width in pixels
        time = hfp['header']['t'][()]*tunit/3600.               # time in hours


        ###get data
        unpoldat = np.copy(hfp['unpol'])                        # NX,NY
        hfp.close()

        ###Correct image orientation
        unpoldat = np.flip(unpoldat.transpose((1,0)),axis=0)

        ### Make a guess at the source based on distance and optionally fall back on mass
        #src = SOURCE_DEFAULT
        #src = "M87"
        if dsource > 4.e25 and dsource < 6.2e25: src = "M87"
        elif dsource > 2.45e22 and dsource < 2.6e22: src = "SgrA"

        # Fill in information according to the source
        if src == "SgrA":
            ra = 266.416816625
            dec = -28.992189444
        elif src == "M87":
            ra = 187.70593075
            dec = 12.391123306

    ###Process image to set proper dimensions
    fovmuas = DX / dsource * lunit * 2.06265e11
    psize_x = eh.RADPERUAS * fovmuas / nx
    Iim = unpoldat*jyscale


    ##important ra is in hours!!!
    return eh.image.Image(Iim, psize_x,  ra * 12./180., dec, rf=rf, source=sourcename, polrep='stokes', pol_prim='I', time=time)


def create_scatter_mod(m_inp):
    if not is_set(m_inp, 'add_scattering') or m_inp.add_scattering=='False' or m_inp.add_scattering=='None':
        return False
    scfile = str(m_inp.add_scattering)
    if not os.path.isfile(scfile):
        raise ValueError('Specified scattering file {0} not found.'.format(scfile))
    scf = open(scfile, 'r')
    scl = scf.readlines()
    scf.close()
    scp = {}
    sck = scl[0].split()
    scw = scl[1].split()
    for j, key in enumerate(sck):
        scp[key] = float(scw[j])
    return eh.scattering.ScatteringModel(**scp)

def scatter_mod(ehim, scattering_mod, sreal=-1, v_x=0.0, v_y=0.0, t_hr=0.0):
    if not scattering_mod:
        return ehim
    Epsilon_Screen = eh.scattering.MakeEpsilonScreen(ehim.xdim, ehim.ydim, sreal)
    return scattering_mod.Scatter(ehim, Epsilon_Screen, Vx_km_per_s=v_x, Vy_km_per_s=v_y, t_hr=t_hr)

def scatter_all(modellist, scattering_mod=False, sreal=-1, v_x=0.0, v_y=0.0, framedur_hour=1.e9, obslen_h=12, ispol=False):
    if not scattering_mod:
        return
    vv_x = v_x
    vv_y = v_y
    if ispol == "True":
        #TODO: Implement timestepping for full pol models
        vv_x = 0.0
        vv_y = 0.0
    else:
        for i, m in enumerate(modellist):
            im  = eh.image.load_image(m)
            if len(modellist) == 1:
                fitspath = os.path.dirname(m)
                for i2 in range(int(obslen_h/fdur_h)):
                    sim = scatter_mod(im, scattering_mod, sreal, vv_x, vv_y, i2*framedur_hour)
                    sim.save_fits('%s/t%06d.fits'%(fitspath, i2))
                os.remove(m)
            else:
                sim = scatter_mod(im, scattering_mod, sreal, vv_x, vv_y, i*framedur_hour)
                os.remove(m)
                sim.save_fits(m)


def convert_ehtim_singlemod(m_inp, imfile, i_num, _outdir, _is_txt=False, fullpol='False', scattering_mod=False, sreal=-1,
                            v_x=0.0, v_y=0.0, t_hr=0.0):
    if _is_txt:
        im = eh.image.load_txt(imfile)
    else:
        if imfile.endswith('hdf5'):
            newname = '.'.join(imfile.split('.')[:-1])+'.h5'
            os.rename(imfile, newname)
            imfile = newname
        try:
            im = eh.image.load_image(imfile)
        except KeyError:
            im = load_singlepol_im_hdf5(imfile)
    im  = scatter_mod(im, scattering_mod, sreal, v_x, v_y, t_hr)
    iim = im.copy()
    iim.qvec = 0*iim.imvec
    iim.uvec = 0*iim.imvec
    iim.vvec = 0*iim.imvec
    if fullpol=='False':
        foutname = '%s/t%06d.fits'%(_outdir, i_num)
        iim.save_fits(foutname)
        rescale_fitsfiles(m_inp, [foutname])
        return
    qim = im.copy()
    if any(qim.qvec):
        qim.imvec = qim.qvec
    else:
        qim.imvec = 0*qim.imvec
    qim.qvec = 0*qim.imvec
    qim.uvec = 0*qim.imvec
    qim.vvec = 0*qim.imvec
    uim = im.copy()
    if any(uim.uvec):
        uim.imvec = uim.uvec
    else:
        uim.imvec = 0*uim.imvec
    uim.qvec = 0*uim.imvec
    uim.uvec = 0*uim.imvec
    uim.vvec = 0*uim.imvec
    vim = im.copy()
    if any(vim.vvec):
        vim.imvec = vim.vvec
    else:
        vim.imvec = 0*vim.imvec
    vim.qvec = 0*vim.imvec
    vim.uvec = 0*vim.imvec
    vim.vvec = 0*vim.imvec
    foutnames = ['%s/t%06d-I-model.fits'%(_outdir, i_num), '%s/t%06d-Q-model.fits'%(_outdir, i_num),
                 '%s/t%06d-U-model.fits'%(_outdir, i_num), '%s/t%06d-V-model.fits'%(_outdir, i_num)]
    iim.save_fits(foutnames[0])
    qim.save_fits(foutnames[1])
    uim.save_fits(foutnames[2])
    vim.save_fits(foutnames[3])
    rescale_fitsfiles(m_inp, foutnames)

def convert_ehtim_hdf5movie(m_inp, _master_input_file, imfile, _outdir, fullpol='False', scattering_mod=False, sreal=-1,
                            v_x=0.0, v_y=0.0):
    fout_id = ''.join(random.choice(string.digits) for digit in range(6))
    h5tofid = _outdir+'/'+fout_id
    h5 = eh.io.load.load_movie_hdf5(imfile)
    h5.save_fits(h5tofid)
    fdur   = np.median(np.diff(h5.times))
    fdur_s = fdur * 3600
    h5tofs = glob.glob(h5tofid+'*')
    natural_sort_Ned_Batchelder(h5tofs)
    for ii, imfimf in enumerate(h5tofs):
        imf2fits = imfimf+'.fits'
        os.rename(imfimf, imf2fits)
        convert_ehtim_singlemod(m_inp, imf2fits, ii, _outdir, False, fullpol, scattering_mod, sreal, v_x, v_y, ii*fdur)
        os.remove(imf2fits)
    alter_line(_master_input_file, 'frameduration', 'frameduration={0}\n'.format(str(fdur_s)))


def rescale_fitsfiles(m_inp, fitslist):
    #Rescale images if requested
    if not isinstance(fitslist, list):
        fitslist = [fitslist]
    if is_set(m_inp, 'model_scale') and '!' in m_inp.model_scale:
        scale_factor = float(m_inp.model_scale.replace('!', ''))
        for fits in fitslist:
            open_fits = astropy.io.fits.open(fits)
            header    = open_fits[0].header
            cdelt1    = header['CDELT1']
            cdelt2    = header['CDELT2']
            open_fits.close()
            with astropy.io.fits.open(fits, 'update') as fits_open:
                for hdu in fits_open:
                    hdu.header['CDELT1'] = cdelt1 * scale_factor
                    hdu.header['CDELT2'] = cdelt2 * scale_factor


master_input_file   = sys.argv[1]
input_fits_location = sys.argv[2]
master_inp = input_reader.read_master_inp(master_input_file)

ScatteringModel = create_scatter_mod(master_inp)
if master_inp.predict_seed == '-1':
    this_seed = 0
else:
    this_seed = int(master_inp.predict_seed)
scatterscreen_vx = 0.0
scatterscreen_vy = 0.0
if is_set(master_inp, 'scattering_vx'):
    scatterscreen_vx = float(master_inp.scattering_vx)
if is_set(master_inp, 'scattering_vy'):
    scatterscreen_vy = float(master_inp.scattering_vy)
minflux   = 0.0
forceflux = False
if is_set(master_inp, 'mod_minflux'):
    if '!' in master_inp.mod_minflux:
        forceflux = True
        minflux   = float(master_inp.mod_minflux.replace('!',''))
    else:
        minflux = float(master_inp.mod_minflux)
fdur_h = float(master_inp.frameduration)/3600.
if master_inp.obs_vex == 'True':
    obslen = obslen_from_vex(master_inp.vexfile)
else:
    obslen = float(master_inp.ms_obslength)

#Set the correct inputs for the master input copy in /tmp/ based on the input image(s) in /tmp/
input_im = master_inp.input_fitsimage
if input_im[-5:] == '.fits':
    print ('>> Input format is fits.')
    fits_list = glob.glob(input_fits_location+'/*.fits')
    rescale_fitsfiles(master_inp, fits_list)
    if minflux:
        Imodels = []
        for im in fits_list:
            if '-I-model.fits' in im:
                Imodels.append(eh.image.load_fits(im).total_flux())
                continue
            elif '-Q-model.fits' in im:
                continue
            elif '-U-model.fits' in im:
                continue
            elif '-V-model.fits' in im:
                continue
            elif '.fits' in im:
                Imodels.append(eh.image.load_fits(im).total_flux())
                continue
        min_modelflux = min(Imodels)
        if min_modelflux < minflux or forceflux:
            cfac = minflux / min_modelflux
            for im in fits_list:
                iim       = eh.image.load_fits(im)
                iim.imvec*= cfac
                iim.save_fits(im)
        else:
            pass
    if len(fits_list) == 1:
        new_fits_home = os.path.dirname(os.path.abspath(fits_list[0]))+'/000000.fits'
        shutil.move(fits_list[0], new_fits_home)
    #Can sort Stokes I files automatically for the user:
    elif master_inp.input_fitspol != "True":
        tmpdirpath = tempfile.mkdtemp(dir='.')
        for frame in fits_list:
            shutil.move(frame, tmpdirpath)
        new_fits_list_sort = glob.glob(tmpdirpath+'/*.fits')
        natural_sort_Ned_Batchelder(new_fits_list_sort)
        for i, frame in enumerate(new_fits_list_sort):
            shutil.move(frame, input_fits_location+'/%06d.fits'%i)
        shutil.rmtree(tmpdirpath)
    scatter_all(glob.glob(input_fits_location+'/*.fits'), ScatteringModel, this_seed, scatterscreen_vx, scatterscreen_vy, fdur_h,
                obslen, master_inp.input_fitspol)
    alter_line(master_input_file, 'input_fitsimage', 'input_fitsimage={0}/*.fits\n'.format(input_fits_location))
    sys.exit()
else:
    if input_im[-4:] == '.txt':
        is_txt = True
    else:
        is_txt = False
    imfiles = glob.glob(input_im)
    natural_sort_Ned_Batchelder(imfiles)
    if is_txt:
        f = open(imfiles[0], 'r')
        lines = f.readlines()
        f.close()
        for line in lines:
            if line[0] != '#':
                entries = len(line.split())
                if entries > 10:
                    print ('>> Input format is MeqSilhouette sky model.')
                    alter_line(master_input_file, 'input_fitsimage', 'input_fitsimage={0}/*.txt\n'.format(input_fits_location))
                    sys.exit()
                else:
                    print ('>> Input format is text image')
                    print ('>> Converting text image(s) to fits...')
                    break

    outdir = input_fits_location + '/convert_to_fits'

    if os.path.isdir(outdir) == False:
        os.makedirs(outdir)

    N_files = len(imfiles)
    for i, imfile_i in enumerate(imfiles):
        # Check if it is a hdf5 movie created by ehtim.
        if i==0 and h5py.is_hdf5(imfile_i) and 'polrep' in h5py.File(imfile_i , 'r')['header'].attrs.keys():
            #Multiple hdf5 movie files make no sense
            convert_ehtim_hdf5movie(master_inp, master_input_file, imfile_i, outdir, master_inp.input_fitspol, ScatteringModel,
                                    this_seed, scatterscreen_vx, scatterscreen_vy
                                   )
        else:
            if ScatteringModel and (scatterscreen_vx or scatterscreen_vy) and N_files==1:
                # Create a movie out of a static source file for the time-dependent scattering.
                for i2 in range(int(obslen/fdur_h)):
                    convert_ehtim_singlemod(master_inp, imfile_i, i2, outdir, is_txt, master_inp.input_fitspol, ScatteringModel,
                                            this_seed, scatterscreen_vx, scatterscreen_vy, i2*fdur_h
                                           )
            else:
                convert_ehtim_singlemod(master_inp, imfile_i, i, outdir, is_txt, master_inp.input_fitspol, ScatteringModel,
                                        this_seed, scatterscreen_vx, scatterscreen_vy, i*fdur_h
                                       )

    alter_line(master_input_file, 'input_fitsimage', 'input_fitsimage=%s/*.fits\n'%outdir)
    print ('>> Done.')

    if minflux:
        imfiles = glob.glob('%s/*.fits'%outdir)
        Imodels = []
        for im in imfiles:
            if '-I-model.fits' in im:
                Imodels.append(eh.image.load_fits(im).total_flux())
                continue
            elif '-Q-model.fits' in im:
                continue
            elif '-U-model.fits' in im:
                continue
            elif '-V-model.fits' in im:
                continue
            elif '.fits' in im:
                Imodels.append(eh.image.load_fits(im).total_flux())
                continue
        min_modelflux = min(Imodels)
        if min_modelflux < minflux or forceflux:
            cfac = minflux / min_modelflux
            for im in imfiles:
                iim       = eh.image.load_fits(im)
                iim.imvec*= cfac
                iim.save_fits(im)
        else:
            sys.exit()
