import casatasks as tasks
import glob
import sys
import os
import shutil
import casatools as casac

antennatable = sys.argv[-2]

todel = sys.argv[-1].split('-')
todel = [int(x) for x in todel]

mytb = casac.table()
mytb.open(antennatable, nomodify=False)
mytb.removerows(todel)
mytb.flush()
mytb.done()
mytb.clearlocks()
