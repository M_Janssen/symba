#!/bin/sh
#Script is described in README.md file.

modelpath=$(readlink -f "$1")
modeldir=$(dirname "$modelpath")
scriptpath=$(dirname "$0")

cd ${scriptpath}/../
docker pull mjanssen2308/symba:latest

printf '\n\n\n>> Now, you can run the pipeline with the ./run_modelobs.sh command!\n'
docker run --name sim.cont --rm --init --env MODELFORSYMBA=${modelpath##*/} -it --network=host -v ${modeldir}:/data mjanssen2308/symba:latest
