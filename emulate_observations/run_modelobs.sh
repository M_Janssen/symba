#!/bin/sh
#Script is described in README.md file.

FITSORTXT="${MODELFORSYMBA##*.}"

cd /data
cp ${MODELFORSYMBA} 000000.${FITSORTXT}
python /usr/local/src/symba/symba_modules/emulateobs_set_masterinp.py /data/000000.${FITSORTXT}
symba /usr/local/src/symba/master_input.txt
