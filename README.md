﻿README file for the  
SYnthetic Measurement creator for long Baseline Arrays (SYMBA).  
By Freek Roelofs and Michael Janssen.  
  
  
Purpose:  
=======  
Generate realistic synthetic VLBI observations.  
  
  
Requirements:  
============  
python astropy.  
python json.  
Install https://github.com/rdeane/MeqSilhouette, checkout the leakage branch, and add the MeqSilhouette/framework folder to PYTHONPATH.  
Install https://github.com/achael/eht-imaging, checkout the dev branch, and add the eht-imaging folder to PYTHONPATH.  
Install https://bitbucket.org/M_Janssen/picard and add the picard/picard folder to PATH and PYTHONPATH.  
CASA version from https://bitbucket.org/M_Janssen/picard README added to PATH.  
symba (https://bitbucket.org/M_Janssen/symba) added to PATH and PYTHONPATH.  
  
Or simply install Docker or Singularity.  
Installation instructions for Docker under Ubuntu can be found under https://docs.docker.com/install/linux/docker-ce/ubuntu/.  
Singularity can be obtained from https://github.com/sylabs/singularity. You will need version 2.6.1 or higher.  
For Ubuntu, a recent enough version can be obtained from http://neuro.debian.net/pkgs/singularity-container.html.  
  
  
Input:  
=====  
Vexfile, source model (fits or txt file), and a list of antennas containing sensitivities,  
local weather parameters, and gain, leakage, and pointing errors.  
  
  
Process:  
=======  
Generate a synthetic observation based on the input, add data corruptions,  
and calibrate the corruptions in the same manner as for a real observation.  
  
  
Output:  
======  
Frequency-averaged full Stokes Measurement Set and UVFITS file.  
Optionally, the data is also imaged.  
  
  
Usage:  
=====  
Set input parameters in master_input.txt and then run ./symba.sh master_input.txt.  
Experienced users can also further customize the input parameters copied from the rPICARD and  
MeqSilhouette installations.  
  
With Docker and a /path/to/working/directory, one can do something like this:  
$ docker run --name vlbisim.cont -it --init --network=host -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY=$DISPLAY -v /path/to/working/directory:/data mjanssen2308/symba:latest  
Or (if there are problems with the user ID, forbidding write access to /data in the container)  
$ docker run --name vlbisim.cont -it --init --env HOME=/data --user $(id -u) -v /etc/passwd:/etc/passwd --network=host -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY=$DISPLAY -v /path/to/working/directory:/data mjanssen2308/symba:latest  
  
Note that Docker can be run in root-less mode since version 19.03. Also, Singularity can be used  
instead of Docker with  
$singularity build vlbi.sim docker://mjanssen2308/symba:latest  
$singularity run --cleanenv ./vlbi.sim  
Here, the present working directory will be shared. Other directories can be shared with -B.  
Due to the process in which a singularity image is build from a docker container, problems may  
arise if you have casacore installed on your system. In that case, you will need to keep casacore  
up-to-date to account for leap seconds. Instructions are given in symba/documentation.  
  
  
Emulate Observations:  
====================  
SYMBA is able to exactly reproduce real observations. Users can take their theoretical models and  
investigate how these models would look like when observed, taking into account all instrumental  
effects. The only requirement is that Docker is installed. For interesting cases, example scripts  
are included in the emulate_observations/ folder to make the creation of synthetic data very easy  
for the user. After executing these scripts, the user is inside a Docker container and can run the  
pipeline with $./run_modelobs.sh. Scripts for the following observations are included in the  
emulate_observations/ folder:  
  
- $./EHT2017_M87_e17e11.sh INPUTFITS  
will mimic the ETH 2017 observations of M87 for an INPUTFITS model file. INPUTFITS must be a  
Stokes I fits file ending with a .fits extension or a .txt file (see examples in symba_input/).  
The pipeline will produce an image and UVFITS files (M87_calibrated_netcal_uvflagged.uvf and  
M87_fiducial_ehtim.uvfits after self-calibration and scan-averaging) in the directory of the  
INPUTFITS file. The visibilities from the UVFITS files are also stored in corresponding .txt files.  
The output can be compared directly to the released EHT data for M87_2017_101 in  
https://doi.org/10.25739/g85n-f134.
