FROM ubuntu:16.04

RUN useradd -ms /bin/bash jeanluc \
  && apt-get -qq update \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y ca-certificates \
     software-properties-common apt-utils


RUN add-apt-repository -s ppa:kernsuite/kern-3 \
  && apt-get -qq update \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y ca-certificates \
     vim git curl wget less locales build-essential time pgplot5 pyxis wsclean meqtrees \
     libfreetype6 libsm6 libxi6 libxrender1 libxrandr2 libxfixes3 \
     libxcursor1 libxinerama1 libfontconfig1 libxslt1.1 xauth xvfb dbus-x11 \
     libfontconfig-dev libboost-all-dev libbz2-dev libc6 libtinfo5 \
     python python-matplotlib python-tk python-casacore python-future python-setuptools \
     python-pyxis python python-termcolor python-pynfft python-ephem python-dev \
     python-tigger ipython python-pip \
  && pip install --upgrade pip==9.0.1 \
  && pip install python-irodsclient \
  && pip install decorator==4.4.2 \
  && pip install numpy==1.13.3 \
  && pip install astropy==2.0.16 \
  && pip install scikit-image==0.14.0 \
  && pip install mpltools==0.2.0 \
  && pip install seaborn==0.9.0 \
  && pip install h5py \
  && pip install requests \
  && pip install pandas==0.23.3 \
  && pip install natsort==6.2.1 \
  && mkdir /root/.irods && \
    echo '\
{\
    "irods_host": "data.cyverse.org",\
    "irods_port": 1247,\
    "irods_user_name": "anonymous",\
    "irods_zone_name": "iplant"\
}' > /root/.irods/irods_environment.json \
  && curl -o irods-icommands-4.1.10-ubuntu14-x86_64.deb https://files.renci.org/pub/irods/releases/4.1.10/ubuntu14/irods-icommands-4.1.10-ubuntu14-x86_64.deb \
  && apt-get install --no-install-recommends -y ./irods-icommands-4.1.10-ubuntu14-x86_64.deb \
  && rm -f irods-icommands-4.1.10-ubuntu14-x86_64.deb \
  && apt-get remove -y ca-certificates && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && mkdir -p /u \
  && mkdir -p /ptmp


ADD symba /usr/local/src/symba
ADD picard /usr/local/src/picard


# put symba and dependencies in /usr/local/src,
# get rid of outdated casacore data,
# set locale for CASA perl,
# and create directory for Pegasus
RUN  cd /usr/local/src \
  && curl -k -L "$(cat picard/README.md | grep wget | cut -d' ' -f3)" -o CASA.picard.tar.xz \
  && tar xvJf CASA.picard.tar.xz \
  && rm -rf CASA.picard.tar.xz \
  && mkdir for_picard ; mv casa-* for_picard/ \
  && curl -k -L https://ftp.science.ru.nl/astro/mjanssen/casa-feature-CAS-12325-14.el6.tar.gz -o CASA.picard.tar.gz \
  && tar xfz CASA.picard.tar.gz \
  && rm -rf CASA.picard.tar.gz \
  && CASAVER=$(echo casa-*) \
  && python picard/setup.py -a -p /usr/local/src/for_picard/ \
  && curl -k -L https://ftp.science.ru.nl/astro/mjanssen/aatm-0.5.tar.gz -o aatm-0.5.tar.gz ; tar xfz aatm-0.5.tar.gz ; rm aatm-0.5.tar.gz \
  && cd aatm-0.5 ; ./configure ; make ; make install ; make clean ; cd .. \
  && curl -k -L https://ftp.science.ru.nl/astro/mjanssen/v0.9.7.tar.gz -o v0.9.7.tar.gz ; tar xfz v0.9.7.tar.gz ; rm v0.9.7.tar.gz \
  && cd simms-0.9.7 ; python setup.py install ; cd .. \
  && git clone --single-branch --depth 1 https://github.com/ska-sa/meqtrees-cattery \
  && cd meqtrees-cattery ; python setup.py install ; cd .. \
  && git clone --branch leakage --depth 1 https://github.com/rdeane/MeqSilhouette \
  #&& git clone --single-branch --depth 1 https://github.com/achael/eht-imaging \
  && curl -k -L https://ftp.science.ru.nl/astro/mjanssen/eht-imaging.tar.gz -o eht-imaging.tar.gz ; tar xfz eht-imaging.tar.gz ; rm eht-imaging.tar.gz \
  && chown -R jeanluc /usr/local/src/* \
  && chmod -R 755 /usr/local/src \
  && rm -fr /var/lib/casacore/data ; ln -s /usr/local/src/${CASAVER}/data /var/lib/casacore/data \
  && rm -fr /usr/share/casacore/data ; ln -s /usr/local/src/${CASAVER}/data /usr/share/casacore/data \
  && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
  && dpkg-reconfigure --frontend=noninteractive locales \
  && update-locale LANG=en_US.UTF-8 \
  && mkdir -p /cvmfs
USER jeanluc
WORKDIR /home/jeanluc

# prevent a strange CASA bug,
# download IERS data and attempt to fix weird astropy bug on a few commonly used HPC systems,
# and set paths
RUN  mkdir /home/jeanluc/.matplotlib \
  && touch /home/jeanluc/.matplotlib/tex.cache \
  && chown -R jeanluc /home/jeanluc/.matplotlib ; chmod 755 /home/jeanluc/.matplotlib \
  && export PYTHONHTTPSVERIFY=0 \
  && python -c 'from astropy.time import Time; t = Time("2016:001"); t.ut1' \
  && cp /usr/local/src/symba/emulate_observations/run_modelobs.sh /home/jeanluc/ ; cd \
  && chown -R jeanluc /home/jeanluc/.astropy ; chmod 777 /home/jeanluc/.astropy/cache/download/py2/* \
  && mv /home/jeanluc/.astropy/cache/download/py2/urlmap /home/jeanluc/.astropy/cache/download/ ; cp /home/jeanluc/.astropy/cache/download/py2/* /home/jeanluc/.astropy/cache/download/py2/aa411f40baeb8d531fb10997c36b643e ; cp /home/jeanluc/.astropy/cache/download/py2/aa411f40baeb8d531fb10997c36b643e /home/jeanluc/.astropy/cache/download/py2/656e0fb7e5a207b71ed031b7199cb226 ; mv /home/jeanluc/.astropy/cache/download/urlmap /home/jeanluc/.astropy/cache/download/py2/urlmap
ENV LANG=en_US.UTF-8 \
    PATH=/usr/local/src/picard/picard:/usr/local/src/symba:$PATH \
    PYTHONPATH=/usr/local/src/picard/picard:/usr/local/src/MeqSilhouette:/usr/local/src/eht-imaging:/usr/local/src/symba:$PYTHONPATH \
    MEQS_DIR=/usr/local/src/MeqSilhouette \
    MEQTREES_CATTERY_PATH=/usr/local/src/meqtrees-cattery/Cattery

CMD ["/bin/bash"]



#not required right now:
#libXrandr-dev* libXcursor-dev* libX11-dev* libXinerama-dev* libXext-dev* libXi-dev* libSM-dev* libXrender-dev* libfontconfig libXft* fontconfig-dev* libfontconfig-dev* python-json python-astropy \
#cmake gfortran flex bison libblas-dev liblapacke-dev libcfitsio-dev wcslib-dev
#  && git clone -b 'v2.4.1' --single-branch --depth 1 https://github.com/casacore/casacore \
#  && cd casacore ; mkdir build ; cd build ; cmake .. ; make ; make install ; make clean ; cd ../../ \
#  && wget --no-check-certificate https://github.com/SpheMakh/simms/archive/v0.9.7.tar.gz ; tar xfz v0.9.7.tar.gz ; rm v0.9.7.tar.gz \
#  && wget --no-check-certificate https://launchpad.net/aatm/trunk/0.5/+download/aatm-0.5.tar.gz ; tar xfz aatm-0.5.tar.gz ; rm aatm-0.5.tar.gz \

#setting paths in bashrc:
#  && CASAVER=$(echo casa-*) \
#  && printf '\n# add software to paths\n\
#export PATH=$PATH:/usr/local/src/'$CASAVER'/bin\n\
#export PATH=$PATH:/usr/local/src/picard/picard\n\
#export PYTHONPATH=$PYTHONPATH:/usr/local/src/picard/picard\n\
#export MEQS_DIR=/usr/local/src/MeqSilhouette\n\
#export MEQTREES_CATTERY_PATH=//usr/local/src/meqtrees-cattery/Cattery\n\
#export PYTHONPATH=$PYTHONPATH:/usr/local/src/MeqSilhouette\n\
#export PYTHONPATH=$PYTHONPATH:/usr/local/src/eht-imaging\n\
#export PYTHONPATH=$PYTHONPATH:/usr/local/src/symba\n\
#export PATH=$PATH:/usr/local/src/symba:$PATH\n\
#' >> /etc/bash.bashrc'
