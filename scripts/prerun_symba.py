import os
import sys
import glob
from natsort import natsorted

#TODO read frame duration

# Input parameters
model = sys.argv[1] # Folder containing fits or h5 file(s)
vex_source = sys.argv[2] # Source in vex file to observe
array = sys.argv[3] # 2017, 2020, future
noise = sys.argv[4] # nonoise, thnoise, atmosphere, gains, allnoise, badweather
time_avg = sys.argv[5] # e.g. 10s, 5s
make_image = sys.argv[6] # True, False
predict_seed = sys.argv[7] # Seed for MeqSilhouette corruptions 

# Convert input frame(s) to MeqSilhouette naming convention
# Assumes that fits files are Stokes I only
# h5 can be Stokes I or polarized. Test if this indeed works in case of Stokes I only.
meqfolder = '%s/model_MeqS'%model
if not os.path.isdir(meqfolder):
    os.makedirs(meqfolder)
framelist = glob.glob('%s/*.*'%model)
framelist = natsorted(framelist)
print framelist
modelformat = framelist[0].split('.')[-1]
print modelformat
for i in range(len(framelist)):
    os.system('cp %s %s/%06d.%s'%(framelist[i], meqfolder, i, modelformat))

# Make input/output directories
if not os.path.isdir('./master_inputs'):
    os.makedirs('./master_inputs')
if not os.path.isdir('./symba_output'):
    os.makedirs('./symba_output')
if not os.path.isdir('./symba_output/uvfits'):
    os.makedirs('./symba_output/uvfits')
if not os.path.isdir('./symba_output/images'):
    os.makedirs('./symba_output/images')

# Change master input file 
f = open('master_input.txt', 'r')
g = open('./master_inputs/master_input_%s_%s_%s_%s_%s_%s_%s.txt'%(model, vex_source, array, noise, time_avg, make_image, predict_seed), 'w')
lines = f.readlines()
for i in range(len(lines)):

    # Output directory
    if 'outdirname =' in lines[i]:
        outdir = './symba_output/output_%s_%s_%s_%s_%s_%s'%(model, vex_source, array, noise, time_avg, predict_seed) 
        lines[i] = 'outdirname = %s\n'%outdir

    # Input model
    if 'input_fitsimage =' in lines[i]:
        input_fitsimage = '%s/*.%s'%(meqfolder, modelformat) 
        lines[i] = 'input_fitsimage = %s\n'%input_fitsimage

    # Polarized for h5, Stokes I for fits
    if 'input_fitspol =' in lines[i]:
        if modelformat == 'fits':
            input_fitspol = 'False'
        else:
            input_fitspol = 'True'
        lines[i] = 'input_fitspol = %s\n'%input_fitspol 

    # Antenna table
    if 'ms_antenna_table =' in lines[i]:
        if noise == 'badweather':
            ms_antenna_table = './eht_badweather.antennas'
        else:
            ms_antenna_table = '/usr/local/src/symba/symba_input/VLBIarrays/eht_future.antennas'
        lines[i] = 'ms_antenna_table = %s\n'%(ms_antenna_table) 

    # Observing schedule
    if 'vexfile =' in lines[i]:
        if array == '2017':
            vexlabel = 'e17e11'
        if array == '2020':
            vexlabel = 'e17e11_2020'
        if array == 'future':
            vexlabel = 'e17e11_future'
        vexfile = '/usr/local/src/symba/symba_input/vex_examples/EHT2017/%s.vex'%(vexlabel) 
        lines[i] = 'vexfile = %s\n'%(vexfile)

    # Source name
    if 'vex_source =' in lines[i]:
        lines[i] = 'vex_source = %s\n'%vex_source

    # Thermal noise 
    if 'add_thermal_noise =' in lines[i]:
        if noise == 'nonoise':
            thnoise = 'False'
        else:
            thnoise = 'True'             
        lines[i] = 'add_thermal_noise = %s\n'%thnoise

    # Atmosphere
    if 'trop_enabled =' in lines[i] or 'trop_attenuate =' in lines[i] or 'trop_turbulence =' in lines[i] or 'trop_mean_delay =' in lines[i]:
        if noise == 'atmosphere' or noise == 'allnoise' or noise == 'badweather':
            lines[i] = lines[i].split('=')[0] + '= True\n'
        else:
            lines[i] = lines[i].split('=')[0] + '= False\n'

    # Gains
    if 'pointing_enabled =' in lines[i] or 'uvjones_g_on =' in lines[i] or 'uvjones_d_on =' in lines[i]:
        if noise == 'gains' or noise == 'allnoise' or noise == 'badweather':
            lines[i] = lines[i].split('=')[0] + '= True\n'
        else:
            lines[i] = lines[i].split('=')[0] + '= False\n'

    # Number of channels
    if 'ms_nchan =' in lines[i]:
        if noise == 'nonoise' or noise == 'thnoise' or noise == 'gains':
            ms_nchan = 2
        else:
            ms_nchan = 64
        lines[i] = 'ms_nchan = %s\n'%(ms_nchan) 

    # Integration time
    if 'ms_tint =' in lines[i]:
        if noise == 'nonoise' or noise == 'thnoise' or noise == 'gains':
            ms_tint = 10
        else:
            ms_tint = 0.5
        lines[i] = 'ms_tint = %s\n'%(ms_tint) 

    # rPICARD steps
    if 'rpicard_fullpipeline =' in lines[i]:
        if noise == 'nonoise' or noise == 'thnoise' or noise == 'gains':
            rpicard_fullpipeline = 'False'
        else:
            rpicard_fullpipeline = 'True'
        lines[i] = 'rpicard_fullpipeline = %s\n'%rpicard_fullpipeline

    # Network calibration
    if 'do_netcal =' in lines[i]:
        if noise == 'nonoise' or noise == 'thnoise' or vex_source == 'SGRA':
            do_netcal = 'False'
        else:
            do_netcal = 'True'
        lines[i] = 'do_netcal = %s\n'%do_netcal

    # Final averaging time
    if 'time_avg =' in lines[i]:
        lines[i] = 'time_avg = %s\n'%(time_avg)

    # Make image
    if 'make_image =' in lines[i]:
        lines[i] = 'make_image = %s\n'%(make_image)

    # Seed
    if 'predict_seed =' in lines[i]:
        lines[i] = 'predict_seed = %s\n'%(predict_seed)

    # Only use real data uvfits for 2017 coverage
    if 'match_uv =' in lines[i]:
        if array == '2017':
            match_uv = 'True'
        else:
            match_uv = 'False'
        lines[i] = 'match_uv = %s\n'%(match_uv)    

    g.write('%s'%lines[i])
g.close()
f.close()

# Run pipeline
os.system('symba ./master_inputs/master_input_%s_%s_%s_%s_%s_%s_%s.txt'%(model, vex_source, array, noise, time_avg, make_image, predict_seed))

# Copy output uvfits to common directory
if array == '2017':
    os.system('cp %s/*_uvflagged.uvf ./symba_output/uvfits/%s_%s_%s_%s_%s_%s.uvfits'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))
elif noise == 'thnoise' or noise == 'nonoise' or vex_source == 'SGRA':
    os.system('cp %s/*_calibrated.uvf ./symba_output/uvfits/%s_%s_%s_%s_%s_%s.uvfits'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))
else:
    os.system('cp %s/*_calibrated_netcal.uvf ./symba_output/uvfits/%s_%s_%s_%s_%s_%s.uvfits'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))

# Copy output images to common directory
if make_image == 'True':
    os.system('cp %s/*_fiducial_ehtim.fits ./symba_output/images/%s_%s_%s_%s_%s_%s.fits'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))
    os.system('cp %s/*_fiducial_ehtim.pdf ./symba_output/images/%s_%s_%s_%s_%s_%s.pdf'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))
    os.system('cp %s/*_fiducial_ehtim.uvfits ./symba_output/images/%s_%s_%s_%s_%s_%s_selfcal.uvfits'%(outdir, model, vex_source, array, noise, time_avg, predict_seed))







