#!/usr/bin/env python
#Must have SYMBA installed as given in the PATH_TO_SYMBA variable.
import os
import sys
import random
import symba_modules.generate_input_python as gen_inp
#TODO:
# - add option to include Faraday rotation + option to not avg the channels (avg_chan)
# - add scattering to input models of Sgr A*


### OUTDATED: The OSG/CyVerse workflow is hosted on https://github.com/bhpire/symba-osg now.


PATH_TO_SYMBA = '/home/ckc/symba'
inpd          = 'inputfiles'

def irods_dir_search_dpsaltis(LibraryDirectory, KeyWords=['Ma+0.5', 'Rhigh_1/'], mod_num_select=[], rand_mod_num_sel=False, seed=0):
    """
    Script from Dimitrios.
    Default: Mad[M] models with spin[a] of +0.5 and Rhigh=1 (w/o the slash for Rhigh you would also get Rhigh=10,160,...[1*]).
    """
    import ssl
    from irods.session import iRODSSession
    from irods.models import Collection, DataObject

    random.seed(seed)

    paths = []

    # Get the information on how to connect to CyVerse
    try:
        env_file = os.environ['IRODS_ENVIRONMENT_FILE']
    except KeyError:
        env_file = os.path.expanduser('~/.irods/irods_environment.json')

    # Connect to CyVerse
    ssl_context  = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)
    ssl_settings = {'ssl_context': ssl_context}
    with iRODSSession(irods_env_file=env_file, **ssl_settings) as session:
    # first get the directory structure under the main directory
        coll0 = session.collections.get(LibraryDirectory)

        # and then go recursively through 4 layers of directorieas
        # TODO: rewrite as a true recursion
        for col0items in coll0.subcollections:
            c1name=LibraryDirectory+"/"+col0items.name
            col1=session.collections.get(c1name)
            for col1items in col1.subcollections:
                c2name=c1name+"/"+col1items.name
                col2=session.collections.get(c2name)
                for col2items in col2.subcollections:
                    c3name=c2name+"/"+col2items.name
                    col3=session.collections.get(c3name)
                    for col3items in col3.subcollections:
                        c4name=c3name+"/"+col3items.name
                        col4=session.collections.get(c4name)
                        for col4items in col4.subcollections:

                            pathname=col4items.path
                            if all(ext in pathname for ext in KeyWords):
                                if rand_mod_num_sel:
                                    these_mods = []
                                    for _ in range(rand_mod_num_sel[0]):
                                        these_mods.append(int(random.uniform(0, rand_mod_num_sel[1])))
                                else:
                                    these_mods = mod_num_select
                                if these_mods:
                                    for mod_num in these_mods:
                                        try:
                                            paths.append(pathname+'/'+col4items.data_objects[mod_num].name)
                                        except IndexError:
                                            pass
                                else:
                                    paths.append(pathname)
    return paths


##################    INPUT PARAMETERS    ########################################################################################################
seednoise         = 0 #0 to give each submission a different number
realizations      = 3 #number of different realizations for noise and corruptions for each model
keep_redundant    = False #also copy back redundant files such as the MS back to CyVerse
frameduration     = '9999999999999999' #duration of a single model frame in s; TODO: read this from the library itself
mod_rotation      = 288 #rotate models by this amount [deg]
mod_scale         = 1 #scale models by this factor
reconstruct_image = True #create a simple .fits image reconstruction
proclvl           = 'fringefit' #thermal or fringefit
src               = 'M87' #M87 or SGRA
time_avg          = '10s' #time cadence of exported data, must be given in [s]
N_channels        = 64 #number of spectral channels
avg_chan          = True #used just for filepaths right now; TODO: add option in master_input.txt to not do channel avg
tracks            = ['e17a10', 'e17b06', 'e17d05', 'e17e11'] #Can obs M87 on a10,b06,d05,e11 and SGRA also on c07
match_coverage    = True #strip uv-coverage to the one from a real observation
band              = 'lo' #lo or hi (used to match uv-coverage)
storage_filepath0 = '/iplant/home/shared/eht' #shared top-level dir for simulations and synthetic data
sdir              = 'SynthData' #top-level dir for synthetic data in storage_filepath0
mdir              = 'BkupSimulationLibrary/M87/H5S/suggested/2020.01.06' #top-level dir for simulations in storage_filepath0
smodel_kywrds     = ['a+0.5', 'Rhigh_1/'] #if input_models=[], all filenames with these keywords in storage_filepath0/+mdir are used as input models
mod_num_select    = [100, 300, 500, 700, 900] #set to [] to load all models (time-dependent src) or [a,b,c,...] to load models#a,#b#,#c,... (start counting them at 0) for separate synthetic datasets
rand_mod_num_sel  = [5, 1000] #if not set to False, give [x,y] overwrite mod_num_select to x numbers drawn out of y
input_models      = []
#input_models = ['/iplant/home/shared/eht/BkupSimulationLibrary/M87/H5S/suggested/2020.01.06/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_230/image_Ma+0.5_1156_163_0_230.e9_6.2e9_5.61134e+24_1_320_320.h5',
#                '/iplant/home/shared/eht/BkupSimulationLibrary/M87/H5S/suggested/2020.01.06/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_230/image_Ma+0.5_1212_163_0_230.e9_6.2e9_5.61134e+24_1_320_320.h5',
#                '/iplant/home/shared/eht/BkupSimulationLibrary/M87/H5S/suggested/2020.01.06/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_230/image_Ma+0.5_1344_163_0_230.e9_6.2e9_5.61134e+24_1_320_320.h5',
#                '/iplant/home/shared/eht/BkupSimulationLibrary/M87/H5S/suggested/2020.01.06/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_230/image_Ma+0.5_1686_163_0_230.e9_6.2e9_5.61134e+24_1_320_320.h5',
#               ]
##################################################################################################################################################


if len(input_models) == 0:
    input_models = irods_dir_search_dpsaltis(storage_filepath0+'/'+mdir, smodel_kywrds, mod_num_select, rand_mod_num_sel, seednoise)

N_queue = len(input_models) * len(tracks) * realizations
if N_queue == 0:
    sys.exit('\nGot an empty queue. Exiting.')
gen_inp.alter_line('symba_job_submit', 'queue', 'queue = {0}'.format(str(N_queue)))

if not os.path.exists(inpd):
    os.makedirs(inpd)

#os.system('iticket create write {0}'.format(sys.argv[1]))
cmd_args_inpprep0 = 'python ' + PATH_TO_SYMBA+'/scripts/tableIO.py write '

odir0 = storage_filepath0 + '/' + sdir
if avg_chan:
    freq_res = '1'
else:
    freq_res = str(N_channels)
odir__1 = 'Nch{0}_r{1}_s{2}_fringefit_rz'.format(freq_res, str(mod_rotation), str(mod_scale))
reals   = range(realizations)
counter = 0
for inmod in input_models:
    cmd_args_inpprep = cmd_args_inpprep0
    cmd_args_inpprep+= '-i {0} '.format(inmod)
    cmd_args_inpprep+= '-d {0} '.format(frameduration)
    cmd_args_inpprep+= '-j {0} '.format(str(mod_rotation))
    cmd_args_inpprep+= '-q {0} '.format(str(mod_scale))
    cmd_args_inpprep+= '-r {0} '.format(str(reconstruct_image))
    cmd_args_inpprep+= '-e 3 '
    cmd_args_inpprep+= '-c 2 '
    cmd_args_inpprep+= '-o True '
    cmd_args_inpprep+= '-p 0.85457 '
    cmd_args_inpprep+= '-k {0} '.format(str(keep_redundant))
    cmd_args_inpprep+= '-s {0} '.format(src)
    cmd_args_inpprep+= '-l {0} '.format(proclvl)
    cmd_args_inpprep+= '-t {0} '.format(time_avg)
    cmd_args_inpprep+= '-b {0} '.format(str(N_channels))
    if src != 'SGRA':
        cmd_args_inpprep+= '-w True '
    else:
        cmd_args_inpprep+= '-w False '
    for track in tracks:
        if track.startswith('e17'):
            #TODO: Add cases for EHT2018+
            vexf   = '/usr/local/src/symba/symba_input/vex_examples/EHT2017/{0}.vex'.format(track)
            ants   = '/usr/local/src/symba/symba_input/VLBIarrays/eht.antennas'
            ants_d = '/usr/local/src/symba/symba_input/VLBIarrays/distributions/eht17.d'
            cmd_args_inpprep+= '-x {0} '.format(vexf)
            cmd_args_inpprep+= '-a {0} '.format(ants)
            cmd_args_inpprep+= '-g {0} '.format(ants_d)
            if match_coverage:
                uvf = '{0}/2017_coverage/{1}{2}_{3}_coverage.uvf'.format(odir0, track, band, src)
                cmd_args_inpprep+= '-v {0} '.format(uvf)
            else:
                cmd_args_inpprep+= '-v False '
        for _ in reals:
            if seednoise:
                this_seed = seednoise
            else:
                this_seed = counter
            upload_output = odir0 + '/' + track + band + '_' + src + '/'
            upload_output+= inmod.strip(storage_filepath0) + '/'
            upload_output+= odir__1 + str(this_seed)
            cmd_args_inpprep+= '-u {0} '.format(upload_output)
            cmd_args_inpprep+= '-n {0} '.format(str(this_seed))
            cmd_args_inpprep+= '-f {0}/inp.{1} '.format(inpd, str(counter))
            os.system(cmd_args_inpprep)
            counter+= 1
