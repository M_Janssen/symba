#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Write and read simple a one-line txt file with configuration options for the symba master_input.txt file.
Optimized for OSG and based on Freek's prerun_symba.py script.
"""
import os
import sys
import errno
from optparse import OptionParser


def main():
    usage       = "%prog mode [options]"
    parser      = OptionParser(usage=usage)
    parser.add_option("-m", type="string",dest="master_inpf",default="/usr/local/src/symba/master_input.txt",
                      help=r"Master input file. [Default: %default]")
    parser.add_option("-f", type="string",dest="tableio_f",default="inputfiles/0.inp",
                      help=r"Name of configuration txt file. [Default: %default]")
    parser.add_option("-i", type="string",dest="input_fitsimage",default="/iplant/home/shared/eht/BkupSimulationLibrary/M87/H5S/suggested/2020.01.06/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_230",
                      help=r"Location of input model(s). [Default: %default]")
    parser.add_option("-d", type="string",dest="frameduration",default="999999999999999",
                      help=r"Duration [in s] of a single frame. [Default: %default]")
    parser.add_option("-j", type="string",dest="model_rotangle",default="288",
                      help=r"Rotate input model by this amount [in degrees]. [Default: %default]")
    parser.add_option("-q", type="string",dest="model_scale",default="1.0",
                      help=r"Scale input model by this factor. [Default: %default]")
    parser.add_option("-r", type="string",dest="make_image",default="False",
                      help=r"Reconstruct a model from the synthetic data? [Default: %default]")
    parser.add_option("-e", type="string",dest="elevation_limit",default="3",
                      help=r"Elevation limit [in degrees]. [Default: %default]")
    parser.add_option("-c", type="string",dest="N_cores",default="2",
                      help=r"Number of CPU cores to be used by rPICARD. [Default: %default]")
    parser.add_option("-o", type="string",dest="osg_mode",default="True",
                      help=r"Use OSG-CyVerse infrastructure to do the -i and -u IO by writing custom scripts. [Default: %default]")
    parser.add_option("-p", type="string",dest="quantization_efficiency",default="0.881",
                      help=r"Quantization efficiency factor. [Default: %default]")
    parser.add_option("-w", type="string",dest="do_netcal",default="True",
                      help=r"Do network calibration? [Default: %default]")
    parser.add_option("-k", type="string",dest="keep_redundant",default="True",
                      help=r"Keep intermediate and diagnostic files? [Default: %default]")
    parser.add_option("-s", type="string",dest="vex_source",default="M87",
                      help=r"source to be observed. [Default: %default]")
    parser.add_option("-l", type="string",dest="processing_level",default="fringefit",
                      help=r"Level of processing: thermal(+thermal noise) or fringefit(+atmo+rPICARD). [Default: %default]")
    parser.add_option("-t", type="string",dest="time_avg",default="10s",
                      help=r"Final time-averaging interval, must be specified in s. [Default: %default]")
    parser.add_option("-b", type="string",dest="ms_nchan",default="64",
                      help=r"Number of channels created. [Default: %default]")
    parser.add_option("-x", type="string",dest="vexfile",default="/usr/local/src/symba/symba_input/vex_examples/EHT2017/e17e11.vex",
                      help=r"vex schedule. [Default: %default]")
    parser.add_option("-v", type="string",dest="realdata_uvfits",default="False",
                      help=r"Cut uv-coordinates to the ones in the specified uvfits file; set to False to disable. [Default: %default]")
    parser.add_option("-u", type="string",dest="osg_upload",default="/iplant/home/shared/eht/SynthData/e17e11lo_M87/Ma+0.5/M_6.2e9/i_163_PA_0/Rhigh_1/f_227/Nch64_r288_s1_fringefit21",
                      help=r"Where to upload output if osg_mode is enabled. [Default: %default]")
    parser.add_option("-n", type="string",dest="predict_seed",default="0",
                      help=r"Random number seed for meqsil corruptions. [Default: %default]")
    parser.add_option("-a", type="string",dest="ms_antenna_table",default="/usr/local/src/symba/symba_input/VLBIarrays/eht.antennas",
                      help=r"ms_antenna_table. [Default: %default]")
    parser.add_option("-g", type="string",dest="ms_antenna_dist",default="/usr/local/src/symba/symba_input/VLBIarrays/distributions/eht17.d",
                      help=r"Distributions to draw from to modify ms_antenna_table. [Default: %default]")
    parser.add_option("-z", type="string",dest="scattering_table",default="/usr/local/src/symba/symba_input/scattering/Psaltis_Johnson_2018.txt.default",
                      help=r"scattering_table. [Default: %default]")
    parser.add_option("-y", type="string",dest="scattering_dist",default="/usr/local/src/symba/symba_input/scattering/distributions/Psaltis_Johnson_2018.txt",
                      help=r"Distributions to draw from to modify scattering_table. [Default: %default]")
    parser.add_option("--scattervel", type="string",dest="scattering_velocity_dist",default="[0,0]",
                      help=r"Mean and sigma value used for drawing x and y velocities of the scattering screen. [Default: %default]")
    parser.add_option("--refants", type="string",dest="rpicard_refants",default="AA,LM,SM,PV",
                      help=r"List of reference antennas for fringe-fitting. [Default: %default]")
    parser.add_option("--fringecut", type="string",dest="rpicard_fringecuts",default="3.0",
                      help=r"SNR cuts for fringe-fitting. [Default: %default]")
    parser.add_option("--minflux", type="string",dest="mod_minflux",default="0.0",
                      help=r"Rescale flux of pale models. [Default: %default]")
    parser.add_option("--bandwidth", type="string",dest="ms_dnu",default="2",
                      help=r"Observing bandwidth in GHz. [Default: %default]")
    parser.add_option("--custommodrange", type="string",dest="custom_modrange",default="0",
                      help=r"Use a custom instead of a random selection of input model range. [Default: %default]")
    parser.add_option("--blindupload", type="string",dest="blind_upload",default="False",
                      help=r"Upload only the final uvf file. [Default: %default]")
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    if args[0]=='write':
        write_table(opts)
    else:
        read_table(opts)


def write_table(params):
    """
    Write parameter table based on params dictionary.
    """
    if not os.path.exists(os.path.dirname(params.tableio_f)):
        try:
            os.makedirs(os.path.dirname(params.tableio_f))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    with open(params.tableio_f, 'w') as f:
        for p in vars(params):
            f.write(str(p)+' '+str(getattr(params, p))+'\n')


def read_table(params):
    """
    Adjust parameters in master_inpf based on values in a parameter table.
    """
    import numpy as np
    from collections import defaultdict
    import symba_modules.generate_input_python as gen_inp
    import pipe_modules.auxiliary as auxiliary
    info = {}
    tabn = params.tableio_f
    if not os.path.isfile(tabn):
        #file was copied to workdir
        tabn = [dirlvl for dirlvl in tabn.split('/') if dirlvl][-1]
    if not os.path.isfile(tabn):
        raise IOError('Cannot find tableio_f file {0}'.format(str(params.tableio_f)))
    with open(tabn, 'r')  as f:
        for l in f:
            ll = l.split()
            info[ll[0]] = ll[1]
    for p in info:
        gen_inp.alter_line(params.master_inpf, str(p), '{0}={1}\n'.format(str(p), str(info[p])))
    np.random.seed(int(info['predict_seed']))
    sv_dist = np.array(info['scattering_velocity_dist'].split(','), dtype=float)
    vx      = np.random.normal(sv_dist[0], sv_dist[1])
    vy      = np.random.normal(sv_dist[0], sv_dist[1])
    gen_inp.alter_line(params.master_inpf, 'scattering_vx', 'scattering_vx={0}\n'.format(str(vx)))
    gen_inp.alter_line(params.master_inpf, 'scattering_vy', 'scattering_vy={0}\n'.format(str(vy)))
    processing_level0 = info['processing_level'].split('_')[0]
    if processing_level0 == 'fringefit':
        gen_inp.alter_line(params.master_inpf, 'trop_enabled', 'trop_enabled=True\n')
        gen_inp.alter_line(params.master_inpf, 'trop_attenuate', 'trop_attenuate=True\n')
        gen_inp.alter_line(params.master_inpf, 'trop_turbulence', 'trop_turbulence=True\n')
        gen_inp.alter_line(params.master_inpf, 'trop_mean_delay', 'trop_mean_delay=True\n')
        gen_inp.alter_line(params.master_inpf, 'pointing_enabled', 'pointing_enabled=True\n')
        gen_inp.alter_line(params.master_inpf, 'uvjones_g_on', 'uvjones_g_on=True\n')
        gen_inp.alter_line(params.master_inpf, 'uvjones_d_on', 'uvjones_d_on=True\n')
        gen_inp.alter_line(params.master_inpf, 'rpicard_fullpipeline', 'rpicard_fullpipeline=True\n')
    else:
        gen_inp.alter_line(params.master_inpf, 'trop_enabled', 'trop_enabled=False\n')
        gen_inp.alter_line(params.master_inpf, 'trop_attenuate', 'trop_attenuate=False\n')
        gen_inp.alter_line(params.master_inpf, 'trop_turbulence', 'trop_turbulence=False\n')
        gen_inp.alter_line(params.master_inpf, 'trop_mean_delay', 'trop_mean_delay=False\n')
        gen_inp.alter_line(params.master_inpf, 'pointing_enabled', 'pointing_enabled=False\n')
        gen_inp.alter_line(params.master_inpf, 'rpicard_fullpipeline', 'rpicard_fullpipeline=False\n')
        gen_inp.alter_line(params.master_inpf, 'rpicard_steps', 'rpicard_steps=-rq c,h,i,l\n')
        gen_inp.alter_line(params.master_inpf, 'uvjones_g_on', 'uvjones_g_on=False\n')
        gen_inp.alter_line(params.master_inpf, 'uvjones_d_on', 'uvjones_d_on=False\n')
    if processing_level0 == 'thermal':
        gen_inp.alter_line(params.master_inpf, 'do_netcal', 'do_netcal=False\n')
    elif processing_level0 == 'thermal+gains' or processing_level0 == 'thermal+samegains':
        gen_inp.alter_line(params.master_inpf, 'uvjones_g_on', 'uvjones_g_on=True\n')
    elif processing_level0 == 'thermal+gains+leakage' or processing_level0 == 'thermal+samegains+leakage':
        gen_inp.alter_line(params.master_inpf, 'uvjones_g_on', 'uvjones_g_on=True\n')
        gen_inp.alter_line(params.master_inpf, 'uvjones_d_on', 'uvjones_d_on=True\n')
    if str(info['vexfile']) == 'None' or str(info['vexfile']) == 'False':
        gen_inp.alter_line(params.master_inpf, 'obs_vex', 'obs_vex=False\n')
    if info['osg_mode']=="True":
        #Write scripts that are to be executed on the OSG by the symba_run script.
        with open('osg_move_data.sh', 'w') as f:
            f.write('#!/bin/bash\nset -e\n')
            #Cleanup when a directory with multiple models was obtained.
            f.write('if [ -d input_models/$1 ]; then\n')
            f.write('    mv input_models/*/* input_models/\n')
            f.write('    rm -R `ls -1 -d input_models/*/`\n')
            f.write('fi')
            if info['realdata_uvfits'] != "False":
                uvff0 = info['realdata_uvfits']
                uvff1 = [dirlvl for dirlvl in uvff0.split('/') if dirlvl][-1]
                if os.path.isfile(uvff1):
                    uvff = uvff1
                else:
                    uvff = uvff0
                gen_inp.alter_line(params.master_inpf, 'realdata_uvfits', 'realdata_uvfits={0}\n'.format(str(uvff)))
                gen_inp.alter_line(params.master_inpf, 'match_uv', 'match_uv=True\n')
            else:
                gen_inp.alter_line(params.master_inpf, 'match_uv', 'match_uv=False\n')
        with open('set_inpim.py', 'w') as f:
            f.write("#!/usr/bin/env python\n")
            f.write("import os\n")
            f.write("import sys\n")
            f.write("import random\n")
            f.write("from glob import glob\n")
            f.write("import symba_modules.generate_input_python as gen_inp\n")
            f.write("inpims = glob('input_models/*')\n")
            f.write("for f in inpims:\n")
            f.write("    if not f.endswith('.fits') and not f.endswith('.h5') and not f.endswith('.hdf5'):\n")
            f.write("        os.remove(f)\n")
            f.write("inpims = sorted(glob('input_models/*'))\n")
            f.write("fext   = [ext.split('.')[-1] for ext in inpims]\n")
            f.write("imext  = max(set(fext), key = fext.count)\n")
            f.write("i_f    = 'input_models/*.{0}'.format(str(imext))\n")
            f.write(r"gen_inp.alter_line('"+params.master_inpf+r"', 'input_fitsimage', 'input_fitsimage={0}\n'.format(i_f))")
            f.write("\n")
            f.write("inppol = True\n")
            f.write("if imext == 'fits': inppol = False\n")
            f.write(r"gen_inp.alter_line('"+params.master_inpf+r"', 'input_fitspol', 'input_fitspol={0}\n'.format(inppol))")
            f.write("\n")
            f.write("Nmods = len(inpims)\n")
            f.write("if Nmods > 5:\n")
            #Assume max obs length of 12h
            f.write("    mod_tail = int(43200/"+str(info['frameduration'])+")\n")
            f.write("    max_startmod = Nmods - mod_tail\n")
            f.write("    if max_startmod < 0:\n")
            f.write("        sys.exit()\n")
            f.write("    random.seed("+str(info['predict_seed'])+")\n")
            #Spiel
            f.write("    max_startmod-= 4\n")
            if ',' in info['custom_modrange']:
                custom_modrange = info['custom_modrange'].split(',')
                f.write("    startmod     = "+str(custom_modrange[0]))
                f.write("\n")
                f.write("    endmod       = "+str(custom_modrange[1]))
                f.write("\n")
            else:
                f.write("    startmod     = max(int(random.random() * max_startmod), 0)\n")
                f.write("    endmod       = startmod + mod_tail + 2\n")
            f.write("    modrange     = range(startmod,endmod)\n")
            f.write("    os.system('mkdir -p input_models/tmp')\n")
            f.write("    os.system('mv input_models/*.{0} input_models/tmp/'.format(imext))")
            f.write("\n")
            f.write("    inpims2 = sorted(glob('input_models/tmp/*'))\n")
            f.write("    for i_new, i_orig in enumerate(modrange):\n")
            f.write("        new_mod  = inpims[i_new]\n")
            f.write("        orig_mod = inpims2[i_orig]\n")
            f.write(r"        os.system('mv {0} {1}'.format(orig_mod, new_mod))")
            f.write("\n")
            f.write("    os.system('rm -r input_models/tmp')\n")
            f.write("    with open('inp.modelrange', 'w') as f:\n")
            f.write(r"        f.write('startmodelframe {0} endmodelframe {1}'.format(str(startmod), str(endmod)))")
        gen_inp.alter_line(params.master_inpf, 'keep_rawdata', 'keep_rawdata=False\n')
        with open('osg_cleanup_data.sh', 'w') as f:
            f.write('#!/bin/bash\nset -e\n')
            f.write('mkdir -p symba_output/input/\n')
            if info['make_image']=="True":
                f.write('mv symba_output/*_fiducial_ehtim.fits symba_output/image_reconstruction.fits\n')
            if info['keep_redundant'] == "False":
                f.write('rm -rf symba_output/bhc_synthetic.MS*\n')
                f.write('rm -rf symba_output/diagnostics_*\n')
                f.write('rm -rf symba_output/input/*\n')
                f.write('rm -rf symba_output/antab_concat.antab\n')
                f.write('rm -rf symba_output/*.png\n')
                f.write('rm -rf symba_output/*fiducial_ehtim*\n')
                if info['do_netcal'] == "True":
                    f.write('rm -rf symba_output/*_calibrated.uvf\n')
                    if info['realdata_uvfits'] != "False":
                        f.write('rm -rf symba_output/*_calibrated_netcal.uvf\n')
                    else:
                        pass
                f.write('rm -rf symba_output/*_calibrated_*.txt\n')
            else:
                f.write('mv input_models symba_output/input/\n')
            if info['vex_source'] == 'SGRA' and 'noscattering' not in info['processing_level']:
                f.write('mv scattering.tab symba_output/input/\n')
            if processing_level0 == 'fringefit':
                f.write('mv  symba_output/scan_ptg.gains symba_output/input/\n')
            f.write('mv ms_antenna.tab symba_output/input/\n')
            f.write('mv inp.* symba_output/input/\n')
            if str(info['blind_upload']) == 'True':
                f.write('rm -rf symba_output/input\n')
                f.write('rm -rf symba_output/*.txt\n')
                f.write('rm -rf symba_output/*.fits')
        jcmt_rcp      = ['e17b06', 'e17c07']
        jcmt_lcp      = ['e17a10', 'e17d05', 'e17e11']
        flag_jcmt_pol = False
        if auxiliary.check_array_for_match(info['vexfile'], jcmt_rcp):
            flag_jcmt_pol = True
            with open('jcmt_singlepol.flag', 'w') as f:
                f.write("antenna='JC' correlation = 'L'")
        elif auxiliary.check_array_for_match(info['vexfile'], jcmt_lcp):
            flag_jcmt_pol = True
            with open('jcmt_singlepol.flag', 'w') as f:
                f.write("antenna='JC' correlation = 'R'")
        if flag_jcmt_pol:
            gen_inp.alter_line(params.master_inpf, 'flag_instructions', 'flag_instructions=jcmt_singlepol.flag\n')
        gen_inp.alter_line(params.master_inpf, 'outdirname', 'outdirname=symba_output\n')
        gen_inp.alter_line(params.master_inpf, 'ms_antenna_table', 'ms_antenna_table=ms_antenna.tab\n')
        #Modify the given antenna table based on parameters in a file with distriutions to draw from and the input seed
        antenna_draws = defaultdict(dict)
        col_ids       = {}
        ids_col       = {}
        as_finfo      = open(info['ms_antenna_dist'], 'r')
        as_lines      = as_finfo.readlines()
        as_finfo.close()
        for i, col in enumerate(as_lines[0].split()):
            col_ids[col] = i
            ids_col[i]   = col
        for line in as_lines[1:]:
            lline = line.split()
            st    = lline[col_ids['station']]
            for i, val in enumerate(lline[1:]):
                col = ids_col[i+1]
                #Rejection drawing:
                mwlu      = val.split(',')
                try:
                    # Works only for real numbers.
                    based_on_other_pol     = antenna_draws[st][mwlu[0]]
                    relative               = float(mwlu[1])
                    antenna_draws[st][col] = np.random.normal(based_on_other_pol, relative)
                    continue
                except KeyError:
                    pass
                mean      = complex(mwlu[0])
                width     = complex(mwlu[1])
                lbound    = complex(mwlu[2])
                ubound    = complex(mwlu[3])
                real_draw = rejection_drawing(mean.real, width.real, lbound.real, ubound.real,
                                              info['ms_antenna_dist']
                                             )
                if 'j' not in val:
                    antenna_draws[st][col] = real_draw
                else:
                    imag_draw = rejection_drawing(mean.imag, width.imag, lbound.imag, ubound.imag,
                                                  info['ms_antenna_dist']
                                                 )
                    antenna_draws[st][col] = complex(real_draw, imag_draw)
        if 'samegains' in processing_level0:
            for st in antenna_draws:
                try:
                    antenna_draws[st]['gainL_real'] = antenna_draws[st]['gainR_real']
                except KeyError:
                    pass
        ant_tabf = open(info['ms_antenna_table'], 'r')
        ant_tab  = ant_tabf.readlines()
        ant_tabf.close()
        col_ids  = {}
        for i, col in enumerate(ant_tab[0].split()):
            col_ids[col] = i
        ant_tabf_upd = open('ms_antenna.tab', 'w')
        ant_tabf_upd.write('  '.join(ant_tab[0].split())+'\n')
        for line in ant_tab[1:]:
            mod_line = line.split()
            this_ant = mod_line[0]
            if this_ant in antenna_draws.keys():
                for col in antenna_draws[this_ant].keys():
                    mod_line[col_ids[col]] = str(antenna_draws[this_ant][col])
            else:
                pass
            ant_tabf_upd.write('  '.join(mod_line)+'\n')
        ant_tabf_upd.close()
        if info['vex_source'] == 'SGRA' and 'noscattering' not in info['processing_level']:
            gen_inp.alter_line(params.master_inpf, 'add_scattering', 'add_scattering=scattering.tab\n')
            scatter_draws = {}
            src_distance  = 0
            col_ids       = {}
            ids_col       = {}
            s_finfo       = open(info['scattering_dist'], 'r')
            s_lines       = s_finfo.readlines()
            s_finfo.close()
            for i, col in enumerate(s_lines[0].split()):
                col_ids[col] = i
                ids_col[i]   = col
            lline = s_lines[1].split()
            for i, val in enumerate(lline):
                col = ids_col[i]
                #Rejection drawing:
                mwlu = val.split(',')
                mean = float(mwlu[0])
                if col == 'source_screen_distance':
                    # Follows from known distance to SgrA* and observer_screen_distance
                    src_distance += mean
                    continue
                elif col == 'observer_screen_distance':
                    src_distance += mean
                width              = float(mwlu[1])
                lbound             = float(mwlu[2])
                ubound             = float(mwlu[3])
                scatter_draws[col] = rejection_drawing(mean, width, lbound, ubound, info['scattering_dist'])
            scatter_draws['source_screen_distance'] = src_distance - scatter_draws['observer_screen_distance']
            s_tabf = open(info['scattering_table'], 'r')
            s_tab  = s_tabf.readlines()
            s_tabf.close()
            col_ids = {}
            for i, col in enumerate(s_tab[0].split()):
                col_ids[col] = i
            s_tabf_upd = open('scattering.tab', 'w')
            s_tabf_upd.write('  '.join(s_tab[0].split())+'\n')
            mod_line = s_tab[1].split()
            for col in scatter_draws.keys():
                mod_line[col_ids[col]] = str(scatter_draws[col])
            s_tabf_upd.write('  '.join(mod_line))
            s_tabf_upd.close()


def rejection_drawing(mean, width, lbound, ubound, infile):
    import numpy as np
    safety = 0
    while True:
        drawv = np.random.normal(mean, width)
        if drawv > lbound and drawv < ubound:
            return drawv
        safety += 1
        if safety > 1337:
            raise OverflowError('Stuck in loop. Check parameters in {0} file.'.format(infile))


if __name__=="__main__":
    main()
